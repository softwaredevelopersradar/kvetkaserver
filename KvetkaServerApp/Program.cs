﻿using ServerKvetkaClass;
using System.Diagnostics;

namespace KvetkaServerApp
{
    class Program
    {

        private static ServerKvetkaMainClass _ServerKvetkaMainClass = new();

        static void Main(string[] args)
        {
            //Process.GetCurrentProcess().PriorityClass = ProcessPriorityClass.High;

            if (Instance.HasRunningCopy)
            {
                System.Console.WriteLine("Oops");
                return;
            }

            _ServerKvetkaMainClass.Start();

            do
            {
                System.Console.ReadLine();
            }
            while (true);
        }
    }
}
