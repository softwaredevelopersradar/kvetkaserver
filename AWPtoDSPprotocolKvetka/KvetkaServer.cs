﻿using llcssKvetka;
using Nito.AsyncEx;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Threading.Tasks;
using System.Net;
using KvetkaProtocol;

namespace AWPtoDSPprotocolKvetka
{
    public class KvetkaServer
    {
        private TcpListener listener;

        private List<KvetkaClientObject> ListOfClientObjects = new();

        public delegate void ClientAfterConnectCommandsIsNeeded(KvetkaClientObject kvetkaClientObject);
        public event ClientAfterConnectCommandsIsNeeded ConnectCommandsIsNeeded;

        public delegate void ObjectModeResponseNeeded(KvetkaClientObject kvetkaClientObject, ModeMessage modeMessage);
        public event ObjectModeResponseNeeded ObjectModeResponseIsNeeded;

        public delegate void ObjectSpectrumResponseNeeded(KvetkaClientObject kvetkaClientObject, SpectrumRequest spectrumRequest);
        public event ObjectSpectrumResponseNeeded ObjectSpectrumResponseIsNeeded;

        public delegate void ObjectAndIBinarySerializableResponse(KvetkaClientObject kvetkaClientObject, IBinarySerializable T);
        public event ObjectAndIBinarySerializableResponse ObjectAndIBinarySerializableResponseIsNeeded;

        public delegate void ObjectWaterfallResponseNeeded(KvetkaClientObject kvetkaClientObject, WaterfallRequest waterfallRequest);
        public event ObjectWaterfallResponseNeeded ObjectWaterfallResponseIsNeeded;

        public delegate void ObjectDirFppResponceNeeded(KvetkaClientObject kvetkaClientObject, DirFppResponce dirFppRequest);
        public event ObjectDirFppResponceNeeded ObjectDirFppResponceIsNeeded;

        public async Task ServerStart(string IP, int port)
        {
            try
            {
                //Пробуем стартовать сервер
                listener = new TcpListener(IPAddress.Parse(IP), port);
                listener.Start();
                Console.WriteLine($"Server started on {IP}:{port}");
                Console.WriteLine("Waiting for connections...");
            }
            catch
            {
                listener = new TcpListener(IPAddress.Parse("127.0.0.1"), port);
                listener.Start();
                Console.WriteLine($"Server started on {IPAddress.Parse("127.0.0.1")}:{port}");
                Console.WriteLine("Waiting for connections...");
            }
            while (true)
            {
                TcpClient client = await listener.AcceptTcpClientAsync();
                KvetkaClientObject clientObject = new(client);
                ListOfClientObjects.Add(clientObject);

                clientObject.ClientAdress = ListOfClientObjects.Count;

                InitClientObjectEvents(ref clientObject);

                Console.WriteLine("Client connected");

                //создаем новый поток для обслуживания нового клиента
                Task.Run(() => clientObject.Read());
                //var answer = await ServerSendAddressIdentificationRequest();

                ConnectCommandsIsNeeded?.Invoke(clientObject);
            }
        }

        private void InitClientObjectEvents(ref KvetkaClientObject clientObject)
        {
            clientObject.ClientObjectAndModeMessage += ClientObject_ClientObjectAndModeMessage;

            clientObject.ClientObjectSpectrumResponse += ClientObject_ClientObjectSpectrumResponse;

            clientObject.ClientObjectAndIBinarySerializableResponse += ClientObject_ClientObjectAndIBinarySerializableResponse;

            clientObject.ClientObjectWaterfallResponse += ClientObject_ClientObjectWaterfallResponse;

            clientObject.ClientDirFppResponce += ClientObject_ClientDirFppResponce;
        }

        private void ClientObject_ClientObjectAndModeMessage(KvetkaClientObject kvetkaClientObject, ModeMessage modeMessage)
        {
            if (kvetkaClientObject != null && modeMessage != null)
                ObjectModeResponseIsNeeded?.Invoke(kvetkaClientObject, modeMessage);
        }

        private void ClientObject_ClientObjectSpectrumResponse(KvetkaClientObject kvetkaClientObject, SpectrumRequest spectrumRequest)
        {
            if (kvetkaClientObject != null && spectrumRequest != null)
                ObjectSpectrumResponseIsNeeded?.Invoke(kvetkaClientObject, spectrumRequest);
        }

        private void ClientObject_ClientObjectAndIBinarySerializableResponse(KvetkaClientObject kvetkaClientObject, IBinarySerializable T)
        {
            switch (T)
            {
                case FiltersMessage:
                    break;
                case SettingsMessage:
                    break;
            }

            ObjectAndIBinarySerializableResponseIsNeeded?.Invoke(kvetkaClientObject, T);
        }

        private void ClientObject_ClientObjectWaterfallResponse(KvetkaClientObject kvetkaClientObject, WaterfallRequest waterfallRequest)
        {
            ObjectWaterfallResponseIsNeeded?.Invoke(kvetkaClientObject, waterfallRequest);
        }

        private void ClientObject_ClientDirFppResponce(KvetkaClientObject kvetkaClientObject, DirFppResponce dirFppRequest)
        {
            if (kvetkaClientObject != null /*&& dirFppRequest != null*/)
                ObjectDirFppResponceIsNeeded?.Invoke(kvetkaClientObject, dirFppRequest);
        }

        //Шифр 0
        public async Task<List<DefaultMessage>> ServerSendAddressIdentificationRequest()
        {
            List<DefaultMessage> ListOfDefaultMessages = new();
            for (int i = 0; i < ListOfClientObjects.Count; i++)
            {
                var result = await ListOfClientObjects[i].SendAddressIdentificationRequest(Convert.ToByte(i + 1));
                ListOfDefaultMessages.Add(result);
            }
            return ListOfDefaultMessages;
        }

       

        //Шифр 1
        public async Task<DefaultMessage> ServerSendModeMessage(KvetkaClientObject kvetkaClientObject, ModeMessage modeMessage)
        {
            modeMessage.Header.ReceiverAddress = (byte)(ListOfClientObjects.IndexOf(kvetkaClientObject) + 1);
            modeMessage.Header.Code = 1;

            var result = await kvetkaClientObject.SendModeMessage(modeMessage);
            return result;
        }

        //Шифр 2
        public async Task<DefaultMessage> ServerSendSpectrumResponse(KvetkaClientObject kvetkaClientObject, SpectrumResponse spectrumResponse)
        {
            spectrumResponse.Header.ReceiverAddress = (byte)(ListOfClientObjects.IndexOf(kvetkaClientObject) + 1);
            spectrumResponse.Header.Code = 2;

            var result = await kvetkaClientObject.SendSpectrumResponse(spectrumResponse);
            return result;
        }
        public async Task<List<DefaultMessage>> ServerSendAllSpectrumResponse(SpectrumResponse spectrumResponse)
        {
            List<DefaultMessage> defaultMessages = new List<DefaultMessage>();

            KvetkaClientObject[] localListOfClientObjects;

            lock (ListOfClientObjects)
            {
                localListOfClientObjects = ListOfClientObjects.ToArray();
            }

            spectrumResponse.Header.Code = 2;

            for (int i = 0; i < localListOfClientObjects.Count(); i++)
            {
                spectrumResponse.Header.ReceiverAddress = (byte)(localListOfClientObjects[i].ClientAdress);

                var result = await localListOfClientObjects[i].SendSpectrumResponse(spectrumResponse);

                defaultMessages.Add(result);
            }

            return defaultMessages;
        }

        //Шифр 3
        public async Task<DefaultMessage> ServerSendFiltersMessage(KvetkaClientObject kvetkaClientObject, FiltersMessage filtersMessage)
        {
            filtersMessage.Header.Code = 3;
            filtersMessage.Header.InformationLength = FiltersMessage.BinarySize - MessageHeader.BinarySize;
            filtersMessage.Header.ReceiverAddress = (byte)(ListOfClientObjects.IndexOf(kvetkaClientObject) + 1);

            var result = await kvetkaClientObject.SendFiltersMessage(filtersMessage);
            return result;
        }

        //Шифр 4
        public async Task<DefaultMessage> ServerSendSettingsMessage(KvetkaClientObject kvetkaClientObject, SettingsMessage settingsMessage)
        {
            settingsMessage.Header.Code = 4;
            settingsMessage.Header.InformationLength = SettingsMessage.BinarySize - MessageHeader.BinarySize;
            settingsMessage.Header.ReceiverAddress = (byte)(ListOfClientObjects.IndexOf(kvetkaClientObject) + 1);

            var result = await kvetkaClientObject.SendSettingsMessage(settingsMessage);
            return result;
        }

        //Шифр 5
        public async Task<DefaultMessage> ServerSendWaterfallResponse(KvetkaClientObject kvetkaClientObject, WaterfallResponse waterfallResponse)
        {
            waterfallResponse.Header.Code = 5;
            waterfallResponse.Header.ReceiverAddress = (byte)(ListOfClientObjects.IndexOf(kvetkaClientObject) + 1);

            var result = await kvetkaClientObject.SendWaterfallResponse(waterfallResponse);
            return result;
        }
        public async Task<List<DefaultMessage>> ServerSendAllWaterfallResponse(WaterfallResponse waterfallResponse)
        {
            List<DefaultMessage> defaultMessages = new List<DefaultMessage>();

            KvetkaClientObject[] localListOfClientObjects;

            lock (ListOfClientObjects)
            {
                localListOfClientObjects = ListOfClientObjects.ToArray();
            }

            waterfallResponse.Header.Code = 5;

            for (int i = 0; i < localListOfClientObjects.Count(); i++)
            {
                waterfallResponse.Header.ReceiverAddress = (byte)(localListOfClientObjects[i].ClientAdress);

                var result = await localListOfClientObjects[i].SendWaterfallResponse(waterfallResponse);

                defaultMessages.Add(result);
            }

            return defaultMessages;
        }

        //Шифр 6
        public async Task<List<DefaultMessage>> ServerSendAllFRSMessage(FRSMessage frsMessage)
        {
            List<DefaultMessage> defaultMessages = new List<DefaultMessage>();

            KvetkaClientObject[] localListOfClientObjects;

            lock (ListOfClientObjects)
            {
                localListOfClientObjects = ListOfClientObjects.ToArray();
            }

            frsMessage.Header.Code = 6;

            for (int i = 0; i < localListOfClientObjects.Count(); i++)
            {
                frsMessage.Header.ReceiverAddress = (byte)(localListOfClientObjects[i].ClientAdress);

                var result = await localListOfClientObjects[i].SendFRSMessage(frsMessage);

                defaultMessages.Add(result);
            }

            return defaultMessages;
        }

        //Шифр 7
        public async Task<DefaultMessage> ServerSendISGMessage(KvetkaClientObject kvetkaClientObject, ISGMessage isgMessage)
        {
            isgMessage.Header.Code = 7;
            isgMessage.Header.InformationLength = ISGMessage.BinarySize - MessageHeader.BinarySize;
            isgMessage.Header.ReceiverAddress = (byte)(ListOfClientObjects.IndexOf(kvetkaClientObject) + 1);

            var result = await kvetkaClientObject.SendISGMessage(isgMessage);
            return result;
        }
        public async Task<List<DefaultMessage>> ServerSendAllISGMessage(ISGMessage isgMessage)
        {
            List<DefaultMessage> defaultMessages = new List<DefaultMessage>();

            KvetkaClientObject[] localListOfClientObjects;

            lock (ListOfClientObjects)
            {
                localListOfClientObjects = ListOfClientObjects.ToArray();
            }

            for (int i = 0; i < localListOfClientObjects.Count(); i++)
            {
                var result = await ServerSendISGMessage(localListOfClientObjects[i], isgMessage);

                defaultMessages.Add(result);
            }

            return defaultMessages;
        }

        //Шифр 8
        public async Task<List<DefaultMessage>> ServerSendAllFHSSMessage(FHSSMessage fhssMessage)
        {
            List<DefaultMessage> defaultMessages = new List<DefaultMessage>();

            KvetkaClientObject[] localListOfClientObjects;

            lock (ListOfClientObjects)
            {
                localListOfClientObjects = ListOfClientObjects.ToArray();
            }

            fhssMessage.Header.Code = 8;

            for (int i = 0; i < localListOfClientObjects.Count(); i++)
            {
                fhssMessage.Header.ReceiverAddress = (byte)(localListOfClientObjects[i].ClientAdress);

                var result = await localListOfClientObjects[i].SendFHSSMessage(fhssMessage);

                defaultMessages.Add(result);
            }

            return defaultMessages;
        }

        //Шифр 9
        public async Task<DefaultMessage> ServerSendDirFppResponce(KvetkaClientObject kvetkaClientObject, DirFppResponce dirFppResponce)
        {
            dirFppResponce.Header.ReceiverAddress = (byte)(ListOfClientObjects.IndexOf(kvetkaClientObject) + 1);
            dirFppResponce.Header.Code = 9;

            var result = await kvetkaClientObject.SendDirFppResponce(dirFppResponce);
            return result;
        }
    }

    public class KvetkaClientObject
    {
        public TcpClient kvetkaClient;

        private readonly AsyncLock asyncLock;

        private readonly ConcurrentDictionary<int, ConcurrentQueue<TaskCompletionSource<IBinarySerializable>>> concurrentDictionary;

        public delegate void IsConnectedEventHandler(bool isConnected);
        public event IsConnectedEventHandler ClientIsConnected;

        public delegate void OnReadEventHandler(bool isRead);
        public event OnReadEventHandler ClientIsRead;

        public delegate void IsWriteEventHandler(bool isWrite);
        public event IsWriteEventHandler ClientIsWrite;

        public delegate void ObjectAndModeMessage(KvetkaClientObject kvetkaClientObject, ModeMessage modeMessage);
        public event ObjectAndModeMessage ClientObjectAndModeMessage;

        public delegate void ObjectAndSpectrumResponse(KvetkaClientObject kvetkaClientObject, SpectrumRequest spectrumRequest);
        public event ObjectAndSpectrumResponse ClientObjectSpectrumResponse;

        public delegate void ObjectAndIBinarySerializableResponse(KvetkaClientObject kvetkaClientObject, IBinarySerializable T);
        public event ObjectAndIBinarySerializableResponse ClientObjectAndIBinarySerializableResponse;

        public delegate void ObjectWaterfallResponse(KvetkaClientObject kvetkaClientObject, WaterfallRequest waterfallRequest);
        public event ObjectWaterfallResponse ClientObjectWaterfallResponse;

        public delegate void ObjecDirFppResponce(KvetkaClientObject kvetkaClientObject, DirFppResponce dirFppRequest);
        public event ObjecDirFppResponce ClientDirFppResponce;

        public int ClientAdress { get; set; } = 0;

        public KvetkaClientObject(TcpClient tcpClient)
        {
            kvetkaClient = tcpClient;
            asyncLock = new AsyncLock();
            concurrentDictionary = new ConcurrentDictionary<int, ConcurrentQueue<TaskCompletionSource<IBinarySerializable>>>();
        }


        public async Task Read()
        {
            while (kvetkaClient.Connected)
            {
                var headerBuffer = new byte[MessageHeader.BinarySize];
                var header = new MessageHeader();
                var count = 0;

                try
                {
                    count = await kvetkaClient.GetStream().ReadAsync(headerBuffer.AsMemory(0, MessageHeader.BinarySize)).ConfigureAwait(false);
                    ClientIsRead?.Invoke(true);
                }
                catch (Exception)
                {
                    //Console.WriteLine("Сервер отвалился");
                    ClientIsRead?.Invoke(false);
                    ClientIsConnected?.Invoke(false);
                }
                if (count != MessageHeader.BinarySize)
                {
                    //fatal error
                    ClientIsRead?.Invoke(false);
                    return;
                }

                MessageHeader.TryParse(headerBuffer, out header);

                switch (header.Code)
                {
                    case 0:
                        Console.WriteLine(headerBuffer);
                        break;
                    case 1:
                        var response1 = await ReadResponse<ModeMessage>(header, headerBuffer).ConfigureAwait(false);
                        ClientObjectAndModeMessage?.Invoke(this, response1);
                        break;
                    case 2:
                        var response2 = await ReadResponse<SpectrumRequest>(header, headerBuffer).ConfigureAwait(false);
                        ClientObjectSpectrumResponse?.Invoke(this, response2);
                        break;
                    case 3:
                        var response3 = await ReadResponse<FiltersMessage>(header, headerBuffer).ConfigureAwait(false);
                        ClientObjectAndIBinarySerializableResponse?.Invoke(this, response3);
                        break;
                    case 4:
                        var response4 = await ReadResponse<SettingsMessage>(header, headerBuffer).ConfigureAwait(false);
                        ClientObjectAndIBinarySerializableResponse?.Invoke(this, response4);
                        break;
                    case 5:
                        var response5 = await ReadResponse<WaterfallRequest>(header, headerBuffer).ConfigureAwait(false);
                        ClientObjectWaterfallResponse?.Invoke(this, response5);
                        break;
                    case 9:
                        //var response9 = await ReadResponse<DirFppResponce>(header, headerBuffer).ConfigureAwait(false);
                        ClientDirFppResponce?.Invoke(this, null);
                        break;
                    default:
                        Console.WriteLine("AWP: Unknown Code: " + header.Code);
                        await ReadResponse<DefaultMessage>(header, headerBuffer).ConfigureAwait(false);
                        break;
                }
            }
        }


        private async Task<T> ReadResponse<T>(MessageHeader header, byte[] headerBuffer) where T : class, IBinarySerializable, new()
        {
            var count = 0;
            var buffer = new byte[MessageHeader.BinarySize + header.InformationLength];
            headerBuffer.CopyTo(buffer, 0);

            if (header.InformationLength != 0)
            {
                var difference = header.InformationLength;
                var shiftpos = 0;

                while (difference != 0)
                {
                    count = await kvetkaClient.GetStream().ReadAsync(buffer.AsMemory(MessageHeader.BinarySize + shiftpos, difference)).ConfigureAwait(false);
                    ClientIsRead?.Invoke(true);
                    shiftpos += count;
                    difference -= count;
                }
            }

            try
            {
                var result = new T();
                result.Decode(buffer, 0);
                return result;
            }
            catch (Exception)
            {
                return null;
            }
        }

        static MessageHeader GetMessageHeader(int code, int length)
        {
            return new MessageHeader(0, 1, (byte)code, 0, length);
        }

        private async Task SendRequest(byte[] message)
        {
            using (await asyncLock.LockAsync().ConfigureAwait(false))
            {
                int count = message.Length;
                try
                {
                    await kvetkaClient.GetStream().WriteAsync(message.AsMemory(0, count)).ConfigureAwait(false);
                    ClientIsWrite?.Invoke(true);
                }
                catch (Exception)
                {
                    ClientIsWrite?.Invoke(false);
                }
            }
        }

        //Шифр 0 
        public async Task<DefaultMessage> SendAddressIdentificationRequest(byte ReceiverAddress)
        {
            if (kvetkaClient == null)
                return null;
            if (kvetkaClient.Connected)
            {
                ClientAdress = ReceiverAddress;

                MessageHeader header = GetMessageHeader(code: 0, length: 0);
                header.ReceiverAddress = ReceiverAddress;
                byte[] message = DefaultMessage.ToBinary(header);

                await SendRequest(message).ConfigureAwait(false);
                DefaultMessage answer = new(header);
                return answer;
            }
            else
            {
                MessageHeader header = new(0, 0, 0, 0, 0);
                return new DefaultMessage(header);
            }
        }

        //Шифр 1 
        public async Task<DefaultMessage> SendModeMessage(ModeMessage modeMessage)
        {
            if (kvetkaClient == null)
                return null;
            if (kvetkaClient.Connected)
            {
                byte[] message = ModeMessage.ToBinary(modeMessage.Header, modeMessage.Mode);
                await SendRequest(message).ConfigureAwait(false);
                return new DefaultMessage(modeMessage.Header);
            }
            else
            {
                MessageHeader header = new(0, 0, 1, 1, 0);
                return new DefaultMessage(header);
            }
        }

        //Шифр 2
        public async Task<DefaultMessage> SendSpectrumResponse(SpectrumResponse spectrumResponse)
        {
            if (kvetkaClient == null)
                return null;
            if (kvetkaClient.Connected)
            {
                byte[] message = SpectrumResponse.ToBinary(spectrumResponse.Header, spectrumResponse.Spectrum);
                await SendRequest(message).ConfigureAwait(false);
                return new DefaultMessage(spectrumResponse.Header);
            }
            else
            {
                MessageHeader header = new(0, 0, 2, 1, 0);
                return new DefaultMessage(header);
            }
        }

        //Шифр 3
        public async Task<DefaultMessage> SendFiltersMessage(FiltersMessage filtersMessage)
        {
            if (kvetkaClient == null)
                return null;
            if (kvetkaClient.Connected)
            {
                byte[] message = filtersMessage.GetBytes();
                await SendRequest(message).ConfigureAwait(false);
                return new DefaultMessage(filtersMessage.Header);
            }
            else
            {
                MessageHeader header = new(0, 0, 3, 1, 0);
                return new DefaultMessage(header);
            }
        }

        //Шифр 4
        public async Task<DefaultMessage> SendSettingsMessage(SettingsMessage settingsMessage)
        {
            if (kvetkaClient == null)
                return null;
            if (kvetkaClient.Connected)
            {
                byte[] message = settingsMessage.GetBytes();
                await SendRequest(message).ConfigureAwait(false);
                return new DefaultMessage(settingsMessage.Header);
            }
            else
            {
                MessageHeader header = new(0, 0, 4, 1, 0);
                return new DefaultMessage(header);
            }
        }

        //Шифр 5
        public async Task<DefaultMessage> SendWaterfallResponse(WaterfallResponse waterfallResponse)
        {
            if (kvetkaClient == null)
                return null;
            if (kvetkaClient.Connected)
            {
                byte[] message = waterfallResponse.GetBytes();
                await SendRequest(message).ConfigureAwait(false);
                return new DefaultMessage(waterfallResponse.Header);
            }
            else
            {
                MessageHeader header = new(0, 0, 5, 1, 0);
                return new DefaultMessage(header);
            }
        }

        //Шифр 6
        public async Task<DefaultMessage> SendFRSMessage(FRSMessage frsMessage)
        {
            if (kvetkaClient == null)
                return null;
            if (kvetkaClient.Connected)
            {
                byte[] message = frsMessage.GetBytes();
                await SendRequest(message).ConfigureAwait(false);
                return new DefaultMessage(frsMessage.Header);
            }
            else
            {
                MessageHeader header = new(0, 0, 6, 1, 0);
                return new DefaultMessage(header);
            }
        }

        //Шифр 7
        public async Task<DefaultMessage> SendISGMessage(ISGMessage isgMessage)
        {
            if (kvetkaClient == null)
                return null;
            if (kvetkaClient.Connected)
            {
                byte[] message = isgMessage.GetBytes();
                await SendRequest(message).ConfigureAwait(false);
                return new DefaultMessage(isgMessage.Header);
            }
            else
            {
                MessageHeader header = new(0, 0, 7, 1, 0);
                return new DefaultMessage(header);
            }
        }

        //Шифр 8
        public async Task<DefaultMessage> SendFHSSMessage(FHSSMessage fhssMessage)
        {
            if (kvetkaClient == null)
                return null;
            if (kvetkaClient.Connected)
            {
                byte[] message = fhssMessage.GetBytes();
                await SendRequest(message).ConfigureAwait(false);
                return new DefaultMessage(fhssMessage.Header);
            }
            else
            {
                MessageHeader header = new(0, 0, 8, 1, 0);
                return new DefaultMessage(header);
            }
        }

        //Шифр 9
        public async Task<DefaultMessage> SendDirFppResponce(DirFppResponce dirFppResponce)
        {
            if (kvetkaClient == null)
                return null;
            if (kvetkaClient.Connected)
            {
                byte[] message = dirFppResponce.GetBytes();
                await SendRequest(message).ConfigureAwait(false);
                return new DefaultMessage(dirFppResponce.Header);
            }
            else
            {
                MessageHeader header = new(0, 0, 9, 1, 0);
                return new DefaultMessage(header);
            }
        }
    }
}
