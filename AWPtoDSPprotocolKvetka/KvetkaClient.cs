﻿using KvetkaProtocol;
using llcssKvetka;
using Nito.AsyncEx;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace AWPtoDSPprotocolKvetka
{
    public class KvetkaClient
    {
        private TcpClient client;
        private string IP;
        private const int port = 20003;

        private byte ReceiverAddress;

        private readonly AsyncLock asyncLock;

        private readonly ConcurrentDictionary<int, ConcurrentQueue<TaskCompletionSource<IBinarySerializable>>> concurrentDictionary;

        bool disconnect = false;

        public delegate void IsConnectedEventHandler(bool isConnected);
        public event IsConnectedEventHandler IsConnected;

        public delegate void OnReadEventHandler(bool isRead);
        public event OnReadEventHandler IsRead;

        public delegate void IsWriteEventHandler(bool isWrite);
        public event IsWriteEventHandler IsWrite;

        public KvetkaClient()
        {
            asyncLock = new AsyncLock();
            concurrentDictionary = new ConcurrentDictionary<int, ConcurrentQueue<TaskCompletionSource<IBinarySerializable>>>();
        }

        static byte OwnServerAddress = 0;
        static MessageHeader GetMessageHeader(int code, int length)
        {
            return new MessageHeader(OwnServerAddress, 1, (byte)code, 0, length);
        }

        public async Task ConnectToServer(string IP)
        {
            disconnect = false;
            client = new TcpClient();

            while (!client.Connected)
            {
                try
                {
                    await client.ConnectAsync(IPAddress.Parse(IP), port).ConfigureAwait(false);

                    this.IP = IP;
                    //this.port = port;
                }
                catch (Exception)
                {

                }
                if (!client.Connected)
                    await Task.Delay(500);
            }

            Console.WriteLine("Connect");

            await OnConnected();

            Task.Run(() => ReadThread());

            IsConnected?.Invoke(true);
        }
        public async Task ConnectToServer2(string IP, CancellationToken token)
        {
            disconnect = false;
            client = new TcpClient();

            while (!client.Connected)
            {
                if (token.IsCancellationRequested)
                    return;
                try
                {
                    await client.ConnectAsync(IPAddress.Parse(IP), port).ConfigureAwait(false);

                    this.IP = IP;
                    //this.port = port;
                }
                catch (Exception)
                {

                }
                if (!client.Connected)
                    await Task.Delay(500);
            }

            Console.WriteLine("Connect2");

            Task.Run(() => ReadThread());

            IsConnected?.Invoke(true);
        }

        public async Task<string> IQConnectToServer(string IP, int timeout = 3000)
        {
            Regex regex = new(@"^(25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}|[0-9]{2}|[0-9])(\.(25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}|[0-9]{2}|[0-9])){3}$");
            if (regex.IsMatch(IP, 0))
            {
                CancellationTokenSource cts = new();
                CancellationToken token = cts.Token;

                var task = ConnectToServer2(IP, token);

                if (await Task.WhenAny(task, Task.Delay(timeout)) == task)
                {
                    Console.WriteLine("task completed within timeout");
                    return "OK";
                }
                else
                {
                    cts.Cancel();
                    Console.WriteLine("timeout logic");
                    return "Invalid IP address or Server not found";
                }
            }
            else
            {
                return "Invalid IP address";
            }
        }

        public void DisconnectFromServer()
        {
            disconnect = true;
            if (client != null)
                client.Close();
            IsConnected?.Invoke(false);
        }

        private async Task OnConnected()
        {
            var taskQueues = concurrentDictionary.Values.ToArray();
            foreach (var queue in taskQueues)
            {
                foreach (var task in queue)
                {
                    task.SetResult(null);
                }
            }
            concurrentDictionary.Clear();
        }

        //Core
        private async Task ReadThread()
        {
            while (client.Connected)
            {
                var headerBuffer = new byte[MessageHeader.BinarySize];
                var header = new MessageHeader();
                var count = 0;

                try
                {
                    count = await client.GetStream().ReadAsync(headerBuffer.AsMemory(0, MessageHeader.BinarySize)).ConfigureAwait(false);
                    IsRead?.Invoke(true);
                }
                catch (Exception)
                {
                    //Console.WriteLine("Сервер отвалился");
                    IsRead?.Invoke(false);
                    IsConnected?.Invoke(false);
                    if (disconnect == false)
                        Task.Run(() => ConnectToServer(IP));
                }
                if (count != MessageHeader.BinarySize)
                {
                    //fatal error
                    IsRead?.Invoke(false);
                    return;
                }

                var b00l = MessageHeader.TryParse(headerBuffer, out header);
                if (b00l == false) Console.WriteLine("AWP: Critical Error: .TryParse");

                switch (header.Code)
                {
                    case 0:
                        ReceiverAddress = header.ReceiverAddress;
                        break;
                    case 1:
                        await HandleResponse<ModeMessage>(header, headerBuffer).ConfigureAwait(false);
                        break;
                    case 2:
                        await HandleResponse<SpectrumResponse>(header, headerBuffer).ConfigureAwait(false);
                        break;
                    case 3:
                        if (concurrentDictionary.ContainsKey(header.Code))
                        {
                            await HandleResponse<FiltersMessage>(header, headerBuffer).ConfigureAwait(false);
                        }
                        else
                        {
                            await HandleEvent<FiltersMessage>(header, headerBuffer).ConfigureAwait(false);
                        }
                        break;
                    case 4:
                        if (concurrentDictionary.ContainsKey(header.Code))
                        {
                            await HandleResponse<SettingsMessage>(header, headerBuffer).ConfigureAwait(false);
                        }
                        else
                        {
                            await HandleEvent<SettingsMessage>(header, headerBuffer).ConfigureAwait(false);
                        }
                        break;
                    case 5:
                        await HandleResponse<WaterfallResponse>(header, headerBuffer).ConfigureAwait(false);
                        break;
                    case 6:
                        await HandleEvent<FRSMessage>(header, headerBuffer).ConfigureAwait(false);
                        break;
                    case 7:
                        await HandleEvent<ISGMessage>(header, headerBuffer).ConfigureAwait(false);
                        break;
                    case 8:
                        await HandleEvent<FHSSMessage>(header, headerBuffer).ConfigureAwait(false);
                        break;
                    case 9:
                        await HandleResponse<DirFppResponce>(header, headerBuffer).ConfigureAwait(false);
                        break;
                    default:
                        Console.WriteLine("AWP: Unknown Code: " + header.Code);
                        await ReadResponse<DefaultMessage>(header, headerBuffer).ConfigureAwait(false);
                        break;
                }
            }
        }

        private async Task HandleResponse<T>(MessageHeader header, byte[] headerBuffer) where T : class, IBinarySerializable, new()
        {
            var response = await ReadResponse<T>(header, headerBuffer).ConfigureAwait(false);
            var taskCompletionSource = new TaskCompletionSource<IBinarySerializable>();
            if (!concurrentDictionary[header.Code].TryDequeue(out taskCompletionSource))
            {
                throw new Exception("Response came, but no one is waiting for it");
            }
            if (response != null)
            {
                taskCompletionSource.SetResult(response);
            }
            else
            {
                taskCompletionSource.SetResult(null);
            }
        }

        private async Task<T> ReadResponse<T>(MessageHeader header, byte[] headerBuffer) where T : class, IBinarySerializable, new()
        {
            var count = 0;
            var buffer = new byte[MessageHeader.BinarySize + header.InformationLength];
            headerBuffer.CopyTo(buffer, 0);

            if (header.InformationLength != 0)
            {
                var difference = header.InformationLength;

                var shiftpos = 0;

                try
                {
                    while (difference != 0)
                    {
                        count = await client.GetStream().ReadAsync(buffer.AsMemory(MessageHeader.BinarySize + shiftpos, difference)).ConfigureAwait(false);
                        IsRead?.Invoke(true);
                        shiftpos += count;
                        difference -= count;
                    }
                }
                catch { return null; }
            }

            try
            {
                var result = new T();
                result.Decode(buffer, 0);
                return result;
            }
            catch (Exception e)
            {
                var с = e.ToString();
                return null;
            }
        }


        //Шифр 3 Event
        public delegate void FiltersMessageEventHandler(FiltersMessage filtersMessage);
        public event FiltersMessageEventHandler FiltersMessageEvent;

        //Шифр 4 Event
        public delegate void SettingsMessageEventHandler(SettingsMessage settingsMessage);
        public event SettingsMessageEventHandler SettingsMessageEvent;

        //Шифр 6
        public delegate void FRSMessageEventHandler(FRSMessage frsMessage);
        public event FRSMessageEventHandler FRSMessageEvent;

        //Шифр 7
        public delegate void ISGMessageEventHandler(ISGMessage isgMessage);
        public event ISGMessageEventHandler ISGMessageEvent;

        //Шифр 8
        public delegate void FHSSMessageEventHandler(FHSSMessage fhssMessage);
        public event FHSSMessageEventHandler FHSSMessageEvent;

        private async Task HandleEvent<T>(MessageHeader header, byte[] headerBuffer) where T : class, IBinarySerializable, new()
        {
            var responseEvent = await ReadResponse<T>(header, headerBuffer).ConfigureAwait(false);

            switch (responseEvent)
            {
                //Шифр 3
                case FiltersMessage:
                    FiltersMessageEvent?.Invoke(responseEvent as FiltersMessage);
                    break;
                //Шифр 4
                case SettingsMessage:
                    SettingsMessageEvent?.Invoke(responseEvent as SettingsMessage);
                    break;
                //Шифр 6
                case FRSMessage:
                    FRSMessageEvent?.Invoke(responseEvent as FRSMessage);
                    break;
                //Шифр 7
                case ISGMessage:
                    ISGMessageEvent?.Invoke(responseEvent as ISGMessage);
                    break;
                //Шифр 8
                case FHSSMessage:
                    FHSSMessageEvent?.Invoke(responseEvent as FHSSMessage);
                    break;
            }

            if (responseEvent == null)
            {
                Console.WriteLine("AWP: responseEvent == null");
            }
        }

        private async Task<TaskCompletionSource<IBinarySerializable>> SendRequest(MessageHeader header, byte[] message)
        {
            using (await asyncLock.LockAsync().ConfigureAwait(false))
            {
                var receiveTask = new TaskCompletionSource<IBinarySerializable>();
                if (!concurrentDictionary.ContainsKey(header.Code))
                {
                    concurrentDictionary[header.Code] = new ConcurrentQueue<TaskCompletionSource<IBinarySerializable>>();
                }
                concurrentDictionary[header.Code].Enqueue(receiveTask);

                int count = message.Length;
                try
                {
                    await client.GetStream().WriteAsync(message, 0, count).ConfigureAwait(false);
                    if (IsWrite != null)
                        IsWrite(true);
                }
                catch (Exception)
                {
                    if (IsWrite != null)
                        IsWrite(false);
                    receiveTask.SetException(new Exception("No connection"));
                }

                return receiveTask;
            }
        }

        //Просто отправить
        private async Task SendRequestMini(MessageHeader header, byte[] message)
        {
            using (await asyncLock.LockAsync().ConfigureAwait(false))
            {
                int count = message.Length;
                try
                {
                    await client.GetStream().WriteAsync(message, 0, count).ConfigureAwait(false);
                    IsWrite?.Invoke(true);
                }
                catch (Exception)
                {
                    IsWrite?.Invoke(false);
                }
            }
        }


        //Шифр 1 
        public async Task<ModeMessage> SetMode(KvetkaServerMode mode)
        {
            if (client == null)
                return null;
            if (client.Connected)
            {
                MessageHeader header = GetMessageHeader(code: 1, length: ModeMessage.BinarySize - MessageHeader.BinarySize);
                header.SenderAddress = ReceiverAddress;
                header.ReceiverAddress = 0;

                byte[] message = ModeMessage.ToBinary(header, mode);

                var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
                ModeMessage answer = await taskCompletionSource.Task.ConfigureAwait(false) as ModeMessage;
                return answer;
            }
            //else
            {
                MessageHeader header = new MessageHeader(0, 0, 1, 1, 0);
                ModeMessage answer = new ModeMessage(header, KvetkaServerMode.Stop);
                return answer;
            }
        }

        //Шифр 2
        public async Task<SpectrumResponse> GetSpectrum(byte Mode, int StartFrequencykHzX10, int EndFrequencykHzX10, int StepHz)
        {
            if (client == null)
                return null;
            if (client.Connected)
            {
                MessageHeader header = GetMessageHeader(2, SpectrumRequest.BinarySize - MessageHeader.BinarySize);
                header.SenderAddress = ReceiverAddress;
                header.ReceiverAddress = 0;

                byte[] message = SpectrumRequest.ToBinary(header, Mode, StartFrequencykHzX10, EndFrequencykHzX10, StepHz);

                var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
                var answer = await taskCompletionSource.Task.ConfigureAwait(false) as SpectrumResponse;
                return answer;
            }
            else
            {
                MessageHeader header = new MessageHeader(0, 0, 2, 1, 0);
                SpectrumResponse answer = new SpectrumResponse(header, Array.Empty<byte>());
                return answer;
            }
        }

        //Шифр 3
        public async Task<FiltersMessage> SetFilters(short Threshold)
        {
            if (client == null)
                return null;
            if (client.Connected)
            {
                MessageHeader header = GetMessageHeader(code: 3, length: FiltersMessage.BinarySize - MessageHeader.BinarySize);
                header.SenderAddress = ReceiverAddress;
                header.ReceiverAddress = 0;

                byte[] message = FiltersMessage.ToBinary(header, Threshold);

                var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
                FiltersMessage answer = await taskCompletionSource.Task.ConfigureAwait(false) as FiltersMessage;
                return answer;
            }
            //else
            {
                MessageHeader header = new MessageHeader(0, 0, 3, 1, 0);
                FiltersMessage answer = new FiltersMessage(header, 0);
                return answer;
            }
        }

        //Шифр 4
        public async Task<SettingsMessage> SetSettings(byte Attenuator, byte Amplifier)
        {
            if (client == null)
                return null;
            if (client.Connected)
            {
                MessageHeader header = GetMessageHeader(code: 4, length: SettingsMessage.BinarySize - MessageHeader.BinarySize);
                header.SenderAddress = ReceiverAddress;
                header.ReceiverAddress = 0;

                byte[] message = SettingsMessage.ToBinary(header, Attenuator, Amplifier);

                var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
                SettingsMessage answer = await taskCompletionSource.Task.ConfigureAwait(false) as SettingsMessage;
                return answer;
            }
            //else
            {
                MessageHeader header = new MessageHeader(0, 0, 4, 1, 0);
                SettingsMessage answer = new SettingsMessage(header, 0, 0);
                return answer;
            }
        }

        //Шифр 5
        public async Task<WaterfallResponse> GetWaterfall(byte Mode, int StartFrequencykHzX10, int EndFrequencykHzX10, int StepHz, byte SlicesCount)
        {
            if (client == null)
                return null;
            if (client.Connected)
            {
                MessageHeader header = GetMessageHeader(5, WaterfallRequest.BinarySize - MessageHeader.BinarySize);
                header.SenderAddress = ReceiverAddress;
                header.ReceiverAddress = 0;

                byte[] message = WaterfallRequest.ToBinary(header, Mode, StartFrequencykHzX10, EndFrequencykHzX10, StepHz, SlicesCount);

                var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
                var answer = await taskCompletionSource.Task.ConfigureAwait(false) as WaterfallResponse;
                return answer;
            }
            else
            {
                MessageHeader header = new MessageHeader(0, 0, 5, 1, 0);
                WaterfallResponse answer = new WaterfallResponse(header, 0, Array.Empty<Slice>());
                return answer;
            }
        }

        private MessageHeader CheckCode(byte Code, MessageHeader answerHeader)
        {
            if (answerHeader.Code == 0)
            {
                return new MessageHeader(0, 0, Code, 5, 0);
            }
            else return answerHeader;
        }

        //Шифр 9
        public async Task<DirFppResponce> GetDirFpp()
        {
            if (client == null)
                return null;
            if (client.Connected)
            {
                MessageHeader header = GetMessageHeader(9, 0);
                header.SenderAddress = ReceiverAddress;
                header.ReceiverAddress = 0;

                byte[] message = DefaultMessage.ToBinary(header);

                var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
                var answer = await taskCompletionSource.Task.ConfigureAwait(false) as DirFppResponce;
                return answer;
            }
            else
            {
                MessageHeader header = new MessageHeader(0, 0, 9, 1, 0);
                DirFppResponce answer = new DirFppResponce(header, null);
                return answer;
            }
        }
    }
}
