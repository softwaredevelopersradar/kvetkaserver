﻿using AWPtoDSPprotocolKvetka;
using KvetkaProtocol;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TestKvetkaClient
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        KvetkaClient kvetkaClient = new();

        public MainWindow()
        {
            InitializeComponent();
            kvetkaClient.IQConnectToServer("127.0.0.1", 20003);
        }

        private async void Button_Click0(object sender, RoutedEventArgs e)
        {
            var result = await kvetkaClient.SetMode(KvetkaServerMode.Stop);
            Console.Beep();
        }

        private async void Button_Click1(object sender, RoutedEventArgs e)
        {
            var result = await kvetkaClient.SetMode(KvetkaServerMode.RadioIntelligence);
            Console.Beep();
        }

        private async void Button_Click2(object sender, RoutedEventArgs e)
        {
            var result = await kvetkaClient.SetMode(KvetkaServerMode.RadioJammingFrs);
            Console.Beep();
        }

        private async void Button_Click3(object sender, RoutedEventArgs e)
        {
            var result = await kvetkaClient.SetMode(KvetkaServerMode.RadioJammingFhss);
            Console.Beep();
        }

        private async void Button_Click4(object sender, RoutedEventArgs e)
        {
            var answer = await kvetkaClient.GetSpectrum(0, (int)(10 * 1e4), (int)(20 * 1e4), 0);
            Console.Beep();
        }

        private async void Button_Click9(object sender, RoutedEventArgs e)
        {
            var answer = await kvetkaClient.GetDirFpp();
            Console.Beep();
        }
    }
}
