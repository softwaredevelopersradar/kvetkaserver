﻿using System;

namespace SettingsUtilities
{
    public static class Utilities
    {
        public static float GetFrequencyKhz(int bandNumber, int sampleNumber)
        {
            Contract.Assert(bandNumber >= 0, $"Band number value is {bandNumber}, but it can't be less than zero");
            return Constants.FirstBandMinKhz + Constants.BandwidthKhz * bandNumber + Constants.SamplesGapKhz * sampleNumber;
        }

        public static float GetKFrequencyKhz(int bandNumber, int sampleNumber)
        {
            Contract.Assert(bandNumber >= 0, $"Band number value is {bandNumber}, but it can't be less than zero");

            DspDataModel.FrequencyRange[] frequencyRanges = new DspDataModel.FrequencyRange[5];

            frequencyRanges[0] = new DspDataModel.FrequencyRange(2400000f, 2462500f);
            frequencyRanges[1] = new DspDataModel.FrequencyRange(2437500f, 2500000f);
            frequencyRanges[2] = new DspDataModel.FrequencyRange(5720000f, 5782500f);
            frequencyRanges[3] = new DspDataModel.FrequencyRange(5760000f, 5822500f);
            frequencyRanges[4] = new DspDataModel.FrequencyRange(5807000f, 5870500f);

            //return Constants.FirstBandMinKhz + Constants.BandwidthKhz * bandNumber + Constants.SamplesGapKhz * sampleNumber;

            return frequencyRanges[bandNumber].StartFrequencyKhz + ConstantsK.BandwidthKhz * bandNumber + ConstantsK.SamplesGapKhz * sampleNumber;

        }

        public static float GetK2FrequencyKhz(int bandNumber, int sampleNumber)
        {
            Contract.Assert(bandNumber >= 0, $"Band number value is {bandNumber}, but it can't be less than zero");
            return ConstantsK.FirstBandMinKhz + ConstantsK.BandwidthKhz * bandNumber + ConstantsK.SamplesGapKhz * sampleNumber;
        }

        public static int GetBandNumber(int frequencyKhz)
        {
            Contract.Assert(frequencyKhz >= Constants.FirstBandMinKhz, $"Frequency is {frequencyKhz} KHz, but it can't be less than {Constants.FirstBandMinKhz}");
            return (frequencyKhz - Constants.FirstBandMinKhz) / Constants.BandwidthKhz;
        }

        public static int GetBandNumber(float frequencyKhz)
        {
            Contract.Assert(frequencyKhz >= Constants.FirstBandMinKhz, $"Frequency is {frequencyKhz} KHz, but it can't be less than {Constants.FirstBandMinKhz}");
            return (int)((frequencyKhz - Constants.FirstBandMinKhz) / Constants.BandwidthKhz);
        }

        public static int GetBandNumber(float frequencyKhz, BandBorderSelectionPolicy bandSelectionPolicy, int bandCount)
        {
            var bandNumber = GetBandNumber(frequencyKhz);
            var sampleNumber = GetSampleNumber(frequencyKhz, bandNumber);

            // handle boundary cases
            if (sampleNumber == 0 && bandNumber == 0)
            {
                return 0;
            }
            if (sampleNumber == 0 && bandNumber == bandCount)
            {
                return bandCount - 1;
            }

            // current sample is on the bands border
            if (sampleNumber == 0 && bandSelectionPolicy == BandBorderSelectionPolicy.TakeLeft)
            {
                return bandNumber - 1;
            }
            if (sampleNumber == Constants.BandSampleCount - 1 && bandSelectionPolicy == BandBorderSelectionPolicy.TakeRight)
            {
                return bandNumber + 1;
            }

            return bandNumber;
        }

        public static int GetBandSamplesCount(int receiverSamplesCount)
        {
            return (Constants.BandwidthKhz * (receiverSamplesCount - 1) - receiverSamplesCount + 1) / Constants.ReceiverBandwidthKhz + 2;
        }

        public static int GetBandMinFrequencyKhz(int bandNumber)
        {
            Contract.Assert(bandNumber >= 0, $"Band number value is {bandNumber}, but it can't be less than zero");
            return Constants.FirstBandMinKhz + Constants.BandwidthKhz * bandNumber;
        }

        public static int GetBandMaxFrequencyKhz(int bandNumber)
        {
            Contract.Assert(bandNumber >= 0, $"Band number value is {bandNumber}, but it can't be less than zero");
            return Constants.FirstBandMinKhz + Constants.BandwidthKhz * (bandNumber + 1);
        }

        public static int GetSampleNumber(float frequencyKhz)
        {
            var bandNumber = GetBandNumber(frequencyKhz);
            return GetSampleNumber(frequencyKhz, bandNumber);
        }

        public static int GetSampleNumber(float frequencyKhz, int bandNumber)
        {
            return GetSamplesCount(GetBandMinFrequencyKhz(bandNumber), frequencyKhz);
        }

        /// <summary>
        /// calculates count of samples between two frequencies (for frequencies F and F+sampleGap result is 1)
        /// </summary>
        public static int GetSamplesCount(float startFrequencyKhz, float endFrequencyKhz, float samplesGapKhz = Constants.SamplesGapKhz)
        {
            return (int)((endFrequencyKhz - startFrequencyKhz + samplesGapKhz - 1) / samplesGapKhz);
        }

        private static readonly int[] LiterEdgesKhz =
        {
            50_000,
            90_000,
            160_000,
            290_000,
            512_000,
            860_000,
            1215_000,
            2000_000,
            3000_000
        };

        /// <summary>
        /// Get liter for radio jamming
        /// </summary>
        public static int GetLiter(float frequencyKhz)
        {
            Contract.Assert(
                frequencyKhz >= Constants.MinRadioJamFrequencyKhz,
                $"Frequency is {frequencyKhz} KHz can't be used in radio jamming (min radio jam frequency is {Constants.MinRadioJamFrequencyKhz})");

            if (frequencyKhz < LiterEdgesKhz[0])
            {
                return 1;
            }

            for (var i = 0; i < LiterEdgesKhz.Length; ++i)
            {
                if (frequencyKhz < LiterEdgesKhz[i])
                {
                    return i + 1;
                }
            }
            if (frequencyKhz <= LiterEdgesKhz[LiterEdgesKhz.Length - 1])
            {
                return LiterEdgesKhz.Length;
            }
            throw new ArgumentException($"frequency {frequencyKhz} KHz is to high for radio jam frequency");
        }
    }

    public static class Contract
    {
        public static void Assert(bool condition)
        {
#if DEBUG
            if (!condition)
            {
                throw new Exception("Contract violation!");
            }
#endif
        }

        public static void Assert(bool condition, string errorMessage)
        {
#if DEBUG
            if (!condition)
            {
                throw new Exception(errorMessage);
            }
#endif
        }
    }
    /// <summary>
    /// Selects what band to choose when a sample is lying right on the bands border
    /// </summary>
    /// 
    public static class Constants
    {
        public const int ReceiversCount = 6;

        /// <summary>
        /// Direction finding receivers count
        /// </summary>
        public const int DfReceiversCount = 5;

        public const int PhasesDifferencesCount = DfReceiversCount * (DfReceiversCount - 1) / 2;

        public const int BandwidthKhz = 30000;

        public const float SamplesGapKhz = 1f * ReceiverBandwidthKhz / (ReceiverSampleCount - 1);

        /// <summary>
        /// Count of samples in one band that server take from whole receiver's band
        /// </summary>
        public const int BandSampleCount = (BandwidthKhz * (ReceiverSampleCount - 1) - ReceiverSampleCount + 1) / ReceiverBandwidthKhz + 2;

        public const int DirectionsCount = 360;

        public const int DirectionStep = 1;

        public const int LiterCount = 9;

        public const float ReliabilityThreshold = 0.5f;

        public const int UsrpFirstBandNumber = 100;

        /// <summary>
        /// Khz frequency of first sample in the first band
        /// </summary>
        public const int FirstBandMinKhz = 25000;

        public const int MinRadioJamFrequencyKhz = 30000;

        public const float ReceiverMinAmplitude = -130f;

        /// <summary>
        /// Receiver works inappropriately below this frequency
        /// </summary>
        public const int ReceiverMinWorkFrequencyKhz = 30_000;

        /// <summary>
        ///  Receiver works inappropriately above this frequency
        /// </summary>
        public const int ReceiverMaxWorkFrequencyKhz = 6_000_000;

        /// <summary>
        /// Time in which receiver "should" change band. Need for scan speed calculation
        /// </summary>
        public const float ReceiverBandChangeTime = 1.3f;

        public const float DefaultThresholdValue = -80f;

        /// <summary>
        /// Real receiver's bandwidth
        /// </summary>
        public const int ReceiverBandwidthKhz = 50000;

        /// <summary>
        /// How much samples do receivers really "see" in one band
        /// </summary>
        public const int ReceiverSampleCount = 16384;

        public const int FhssChannelsCount = 4;

        /// <summary>
        /// intersection factor threshold for two fhss networks equality
        /// </summary>
        public const float FhssIntersectionThreshold = 0.7f;

        /// <summary>
        /// Size of peak that is permanently cut off from all scans in intelligence and fhss jamming modes
        /// </summary>
        public const int CenterPeakSampleWidth = 21;

        /// <summary>
        /// Time needed for fpga to collect one sample from air
        /// </summary>
        private const int FpgaSampleCollectionTimeNs = 20;

        public const int FpgaScanCollectionTimeNs = FpgaSampleCollectionTimeNs * ReceiverSampleCount;

        /// <summary>
        /// If number of objectives during rdf is more than this, 
        /// rdf results will be sharded, so we won't wait too long for master processing. 
        /// </summary>
        public const int LinkedRdfShardSize = 8;

        /// <summary>
        /// This constant is used in signal gap calculations
        /// </summary>
        public const float SignalBandwidthToGapMultiplier = 0.25f;

        /// <summary>
        /// This constant is used in signal gap calculations
        /// </summary>
        public const float SignalMaxGapKhz = 300;

        /// <summary>
        /// If we didn't receive rdf result from linked station during this period,
        /// it will be considered "lost", and won't be used in calculations
        /// </summary>
        public const int LinkedRdfResultReceiveTimeout = 1500;

        public const int ApproximateMaxNumberOfStations = 5;

        public const int ExecutiveDfSlaveResponseTimeoutSec = 1000;

        /// <summary>
        /// After this time span ticket is considered to be expired
        /// </summary>
        public const int DatabaseTicketExpirationTimeSec = 5;

        /// <summary>
        /// When we received gps values in n-th time, and n > threshold, we update gps values
        /// even if they are hasn't changed. Because otherwise some programs won't receive correct data
        /// </summary>
        public const int UpdateGpsValuesThreshold = 10;
    }

    public static class ConstantsK
    {
        public const int ReceiversCount = 6;

        /// <summary>
        /// Direction finding receivers count
        /// </summary>
        public const int DfReceiversCount = 5;

        public const int PhasesDifferencesCount = DfReceiversCount * (DfReceiversCount - 1) / 2;

        //public const int BandwidthKhz = 62500;
        public const int BandwidthKhz = 28500;

        public const float SamplesGapKhz = 1f * ReceiverBandwidthKhz / (ReceiverSampleCount - 1);

        /// <summary>
        /// Count of samples in one band that server take from whole receiver's band
        /// </summary>
        public const int BandSampleCount = (BandwidthKhz * (ReceiverSampleCount - 1) - ReceiverSampleCount + 1) / ReceiverBandwidthKhz + 2;

        public const int DirectionsCount = 360;

        public const int DirectionStep = 1;

        public const int LiterCount = 9;

        public const float ReliabilityThreshold = 0.5f;

        public const int UsrpFirstBandNumber = 100;

        /// <summary>
        /// Khz frequency of first sample in the first band
        /// </summary>
        //public const int FirstBandMinKhz = 25000;
        public const int FirstBandMinKhz = 1500;

        public const int MinRadioJamFrequencyKhz = 30000;

        public const float ReceiverMinAmplitude = -130f;

        /// <summary>
        /// Receiver works inappropriately below this frequency
        /// </summary>
        public const int ReceiverMinWorkFrequencyKhz = 1_500;

        /// <summary>
        ///  Receiver works inappropriately above this frequency
        /// </summary>
        public const int ReceiverMaxWorkFrequencyKhz = 30_000;

        /// <summary>
        /// Time in which receiver "should" change band. Need for scan speed calculation
        /// </summary>
        public const float ReceiverBandChangeTime = 1.3f;

        public const float DefaultThresholdValue = -80f;

        /// <summary>
        /// Real receiver's bandwidth
        /// </summary>
        //public const int ReceiverBandwidthKhz = 62500;
        public const int ReceiverBandwidthKhz = 28500;

        /// <summary>
        /// How much samples do receivers really "see" in one band
        /// </summary>
        //public const int ReceiverSampleCount = 8000;
        public const int ReceiverSampleCount = 57000;

        public const int FhssChannelsCount = 4;

        /// <summary>
        /// intersection factor threshold for two fhss networks equality
        /// </summary>
        public const float FhssIntersectionThreshold = 0.7f;

        /// <summary>
        /// Size of peak that is permanently cut off from all scans in intelligence and fhss jamming modes
        /// </summary>
        public const int CenterPeakSampleWidth = 21;

        /// <summary>
        /// Time needed for fpga to collect one sample from air
        /// </summary>
        private const int FpgaSampleCollectionTimeNs = 20;

        public const int FpgaScanCollectionTimeNs = FpgaSampleCollectionTimeNs * ReceiverSampleCount;

        /// <summary>
        /// If number of objectives during rdf is more than this, 
        /// rdf results will be sharded, so we won't wait too long for master processing. 
        /// </summary>
        public const int LinkedRdfShardSize = 8;

        /// <summary>
        /// This constant is used in signal gap calculations
        /// </summary>
        public const float SignalBandwidthToGapMultiplier = 0.25f;

        /// <summary>
        /// This constant is used in signal gap calculations
        /// </summary>
        public const float SignalMaxGapKhz = 300;

        /// <summary>
        /// If we didn't receive rdf result from linked station during this period,
        /// it will be considered "lost", and won't be used in calculations
        /// </summary>
        public const int LinkedRdfResultReceiveTimeout = 1500;

        public const int ApproximateMaxNumberOfStations = 5;

        public const int ExecutiveDfSlaveResponseTimeoutSec = 1000;

        /// <summary>
        /// After this time span ticket is considered to be expired
        /// </summary>
        public const int DatabaseTicketExpirationTimeSec = 5;

        /// <summary>
        /// When we received gps values in n-th time, and n > threshold, we update gps values
        /// even if they are hasn't changed. Because otherwise some programs won't receive correct data
        /// </summary>
        public const int UpdateGpsValuesThreshold = 10;
    }
    public enum BandBorderSelectionPolicy
    {
        TakeLeft, TakeRight
    }
}
