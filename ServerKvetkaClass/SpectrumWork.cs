﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServerKvetkaClass
{
    public static class SpectrumWork
    {
        public interface ICountable
        {
            int Count { get; }
        }

        public interface IAmplitudeBand : ICountable
        {
            float[] Amplitudes { get; }
        }

        public class AmplitudeBand : IAmplitudeBand, ICountable
        {
            public float[] Amplitudes { get; }
            public int Count { get; }

            public AmplitudeBand(float[] amplitudes)
            {
                Amplitudes = amplitudes;
                Count = amplitudes.Count();
            }
        }

        public static IAmplitudeBand StrechSpectrum(this IReadOnlyList<float> inputData, int pointCount)
        {
            // no scaling needed
            if (Math.Abs(inputData.Count - pointCount) <= 1)
            {
                return inputData.Count == pointCount
                    ? new AmplitudeBand(inputData.ToArray())
                    : new AmplitudeBand(inputData.Take(Math.Min(pointCount, inputData.Count)).ToArray());
            }
            var data = new float[pointCount];
            if (inputData.Count < pointCount) // enlarge current data array => using linear extrapolation
            {
                for (var i = 0; i < pointCount; ++i)
                {
                    data[i] = GetExtrapolatedValue(inputData, 1f * i / pointCount);
                }
            }
            else // aggregator.Count > pointCount. reduce current data array => using max on range
            {
                var start = 0;
                var end = start + inputData.Count / pointCount + 1;

                for (var i = 0; i < pointCount; ++i)
                {
                    if (end >= inputData.Count)
                    {
                        end = inputData.Count - 1;
                    }
                    data[i] = GetMaxValueInRange(inputData, start, end);

                    start = end;
                    end = (int)((i + 1f) * inputData.Count / pointCount + 1); // using floats to prevent overflow error
                }
            }
            return new AmplitudeBand(data);
        }

        private static float GetMaxValueInRange(IReadOnlyList<float> data, int start, int end)
        {
            //var to = Math.Min(offset + step, data.Count);
            var answer = data[start];
            for (var i = start + 1; i < end; ++i)
            {
                if (data[i] > answer)
                {
                    answer = data[i];
                }
            }
            return answer;
        }

        /// <summary> calculates linear extrapolated value in current position </summary>
        /// <param name="position"> place in array from 0 to 1 </param>
        private static float GetExtrapolatedValue(IReadOnlyList<float> data, float position)
        {
            var floatIndex = position * data.Count;
            var index1 = (int)floatIndex;
            if (index1 == data.Count - 1)
            {
                return data[index1];
            }
            var index2 = index1 + 1;
            var pos = floatIndex - index1; // position from 0 to 1 between elements with indeces i1 and i2
            var elem1 = data[index1];
            var elem2 = data[index2];

            return elem1 * (1 - pos) + elem2 * pos;
        }
    }
}