﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ServerKvetkaClass
{
    public class Settings : INotifyPropertyChanged
    {
        private WorkMode _workMode = WorkMode.Simulate;
        public WorkMode workMode
        {
            get => _workMode;
            set
            {
                _workMode = value;
                OnPropertyChanged();
            }
        }

        private EndPointConnection _ServerSettings = new(20003);
        public EndPointConnection ServerSettings
        {
            get => _ServerSettings;
            set
            {
                if (_ServerSettings == value)
                    return;
                _ServerSettings = value;
                OnPropertyChanged();
            }
        }

        private EndPointConnection _DBSettings = new(8302);
        public EndPointConnection DBSettings
        {
            get => _DBSettings;
            set
            {
                if (_DBSettings == value)
                    return;
                _DBSettings = value;
                OnPropertyChanged();
            }
        }

        private EndPointConnection _BoraSettings = new(6789);
        public EndPointConnection BoraSettings
        {
            get => _BoraSettings;
            set
            {
                if (_BoraSettings == value)
                    return;
                _BoraSettings = value;
                OnPropertyChanged();
            }
        }

        private EndPointConnection _PelengatorSettings = new(7777);
        public EndPointConnection PelengatorSettings
        {
            get => _PelengatorSettings;
            set
            {
                if (_PelengatorSettings == value)
                    return;
                _PelengatorSettings = value;
                OnPropertyChanged();
            }
        }


        private EndPointConnection _VereskSettings = new(32133);
        public EndPointConnection VereskSettings
        {
            get => _VereskSettings;
            set
            {
                if (_VereskSettings == value)
                    return;
                _VereskSettings = value;
                OnPropertyChanged();
            }
        }
        public Settings()
        {
        }

        #region INotify 
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }
        #endregion
    }

    public interface IMethod<T> where T : class
    {
        T Clone();
        void Update(T data);
        bool Compare(T data);
    }

    public class EndPointConnection : INotifyPropertyChanged, IMethod<EndPointConnection>
    {
        #region NotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }

        #endregion

        #region Private

        private string ipAdress;// = "127.0.0.1";
        private int port;

        #endregion

        #region Properties

        public EndPointConnection()
        {
            ipAdress = "127.0.0.1"; //127.0.0.1
            port = 10000;
        }

        public EndPointConnection(int port)
        {
            ipAdress = "127.0.0.1"; //127.0.0.1
            this.port = port;
        }

        public EndPointConnection(string ipAdress, int port)
        {
            this.ipAdress = ipAdress;
            this.port = port;
        }

        [NotifyParentProperty(true)]
        [DisplayName("IP")]
        [Required]
        [RegularExpression(@"\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b", ErrorMessage = "IP : XXX.XXX.XXX.XXX")]
        public string IpAddress
        {
            get { return ipAdress; }
            set
            {
                if (ipAdress == value)
                    return;
                ipAdress = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        //[Required]
        [Range(1, 1000000)]
        public int Port
        {
            get { return port; }
            set
            {
                if (port == value)
                    return;
                port = value;
                OnPropertyChanged();
            }
        }

        #endregion

        public override string ToString()
        {
            return ipAdress + " : " + port;
        }

        #region IMethod

        public EndPointConnection Clone()
        {
            return new EndPointConnection()
            {
                IpAddress = this.ipAdress,
                Port = this.port
            };
        }

        public void Update(EndPointConnection endPoint)
        {
            IpAddress = endPoint.IpAddress;
            Port = endPoint.Port;
        }

        public bool Compare(EndPointConnection data)
        {
            if (ipAdress != data.IpAddress || port != data.Port)
                return false;
            return true;
        }

        #endregion
    }

    public class UDPEndPointConnection : INotifyPropertyChanged, IMethod<UDPEndPointConnection>
    {

        #region NotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }

        #endregion

        #region Private

        private string _myIpAdress;// = "127.0.0.1";
        private int _myPort;

        private string _remoteIpAdress;// = "127.0.0.1";
        private int _remotePort;

        #endregion

        #region Properties

        public UDPEndPointConnection()
        {
            _myIpAdress = "127.0.0.1"; //127.0.0.1
            _myPort = 10000;

            _remoteIpAdress = "127.0.0.1"; //127.0.0.1
            _remotePort = 10001;
        }

        public UDPEndPointConnection(int myPort, int remotePort)
        {
            _myIpAdress = "127.0.0.1"; //127.0.0.1
            this._myPort = myPort;

            _remoteIpAdress = "127.0.0.1"; //127.0.0.1
            this._remotePort = remotePort;
        }

        [NotifyParentProperty(true)]
        [DisplayName("myIP")]
        [Required]
        [RegularExpression(@"\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b", ErrorMessage = "IP : XXX.XXX.XXX.XXX")]
        public string myIpAdress
        {
            get { return _myIpAdress; }
            set
            {
                if (_myIpAdress == value)
                    return;
                _myIpAdress = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        //[Required]
        [Range(1, 1000000)]
        public int myPort
        {
            get { return _myPort; }
            set
            {
                if (_myPort == value)
                    return;
                _myPort = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        [DisplayName("remoteIP")]
        [Required]
        [RegularExpression(@"\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b", ErrorMessage = "IP : XXX.XXX.XXX.XXX")]
        public string remoteIpAdress
        {
            get { return _remoteIpAdress; }
            set
            {
                if (_remoteIpAdress == value)
                    return;
                _remoteIpAdress = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        //[Required]
        [Range(1, 1000000)]
        public int remotePort
        {
            get { return _remotePort; }
            set
            {
                if (_remotePort == value)
                    return;
                _remotePort = value;
                OnPropertyChanged();
            }
        }

        #endregion

        public override string ToString()
        {
            return myIpAdress + " : " + myPort + " / " + remoteIpAdress + " : " + remotePort;
        }

        #region IMethod

        public UDPEndPointConnection Clone()
        {
            return new UDPEndPointConnection()
            {
                myIpAdress = this._myIpAdress,
                myPort = this._myPort,
                remoteIpAdress = this._remoteIpAdress,
                remotePort = this._remotePort
            };
        }

        public void Update(UDPEndPointConnection udpEndPoint)
        {
            myIpAdress = udpEndPoint.myIpAdress;
            myPort = udpEndPoint.myPort;
            remoteIpAdress = udpEndPoint.remoteIpAdress;
            remotePort = udpEndPoint.remotePort;
        }

        public bool Compare(UDPEndPointConnection data)
        {
            if (
            myIpAdress != data.myIpAdress ||
            myPort != data.myPort ||
            remoteIpAdress != data.remoteIpAdress ||
            remotePort != data.remotePort
            )
                return false;
            return true;
        }

        #endregion
    }

    public class ComConnection : INotifyPropertyChanged, IMethod<ComConnection>
    {
        #region Private

        private string comPort;
        private SpeedPorts portSpeed;

        #endregion

        public ComConnection()
        {
            comPort = "COM1";
            PortSpeed = (SpeedPorts)115200;
        }

        #region Properties
        [NotifyParentProperty(true)]
        public string ComPort
        {
            get => comPort;
            set
            {
                if (comPort == value) return;
                comPort = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        public SpeedPorts PortSpeed
        {
            get => portSpeed;
            set
            {
                if (portSpeed == value) return;
                portSpeed = value;
                OnPropertyChanged();
            }
        }

        #endregion

        public override string ToString()
        {
            return comPort + " / " + (int)PortSpeed;
        }

        #region NotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }

        #endregion

        #region IMethod

        public ComConnection Clone()
        {
            return new ComConnection()
            {
                ComPort = this.comPort,
                PortSpeed = this.portSpeed,
            };
        }

        public void Update(ComConnection comConnection)
        {
            ComPort = comConnection.ComPort;
            PortSpeed = comConnection.PortSpeed;
        }

        public bool Compare(ComConnection data)
        {
            if (comPort != data.ComPort || PortSpeed != data.PortSpeed)
                return false;
            return true;
        }

        #endregion
    }

    public class UniversalConnection : INotifyPropertyChanged, IMethod<UniversalConnection>
    {
        #region Private

        private string comPortOrIP;
        private int portSpeedOrPort;

        #endregion

        public UniversalConnection()
        {
            comPortOrIP = "COM1";
            portSpeedOrPort = 115200;
        }

        #region Properties
        [NotifyParentProperty(true)]
        public string ComPortOrIP
        {
            get => comPortOrIP;
            set
            {
                if (comPortOrIP == value) return;
                comPortOrIP = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        public int PortSpeedOrPort
        {
            get => portSpeedOrPort;
            set
            {
                if (portSpeedOrPort == value) return;
                portSpeedOrPort = value;
                OnPropertyChanged();
            }
        }

        #endregion

        public override string ToString()
        {
            Regex regex = new Regex(@"^(25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}|[0-9]{2}|[0-9])(\.(25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}|[0-9]{2}|[0-9])){3}$");
            if (regex.IsMatch(comPortOrIP, 0))
            {
                return comPortOrIP + ":" + portSpeedOrPort;
            }
            else
            {
                return comPortOrIP + " / " + portSpeedOrPort;
            }
        }

        #region NotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }

        #endregion

        #region IMethod

        public UniversalConnection Clone()
        {
            return new UniversalConnection()
            {
                ComPortOrIP = this.comPortOrIP,
                PortSpeedOrPort = this.portSpeedOrPort,
            };
        }

        public void Update(UniversalConnection comConnection)
        {
            ComPortOrIP = comConnection.ComPortOrIP;
            PortSpeedOrPort = comConnection.PortSpeedOrPort;
        }

        public bool Compare(UniversalConnection data)
        {
            if (comPortOrIP != data.comPortOrIP || portSpeedOrPort != data.portSpeedOrPort)
                return false;
            return true;
        }

        #endregion
    }

    public enum SpeedPorts : int
    {
        [Description("2400")]
        Speed_2400 = 2400,
        [Description("4800")]
        Speed_4800 = 4800,
        [Description("9600")]
        Speed_9600 = 9600,
        [Description("19200")]
        Speed_19200 = 19200,
        [Description("38400")]
        Speed_38400 = 38400,
        [Description("57600")]
        Speed_57600 = 57600,
        [Description("115200")]
        Speed_115200 = 115200
    }

    public enum WorkMode : byte
    {
        Simulate,
        Work
    }
}
