﻿using AWPtoDSPprotocolKvetka;
using KvetkaProtocol;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ServerKvetkaClass
{
    public abstract partial class IServerController
    {
        protected readonly KvetkaServer KvetkaServer;

        public IServerController(KvetkaServer kvetkaServer)
        {
            KvetkaServer = kvetkaServer;
        }

        public float MinFreqMHz { get; set; } = 1.5f;
        public float MaxFreqMHz { get; set; } = 30;

        public virtual async Task<KvetkaServerMode> ModeSwitch(KvetkaServerMode requestServerMode, CancellationToken token)
        {
            switch (requestServerMode)
            {
                case KvetkaServerMode.Stop:
                    break;
                case KvetkaServerMode.RadioIntelligence:
                    await RadioIntelligenceLoop(token);
                    break;
                case KvetkaServerMode.RadioJammingFrs:
                    await RadioJammingFrsLoop(token);
                    break;
                case KvetkaServerMode.RadioJammingFhss:
                    await RadioJammingFhssLoop(token);
                    break;
                default:
                    break;
            }
            return requestServerMode;
        }

        public virtual async Task RadioIntelligenceLoop(CancellationToken token)
        {
            await Task.Delay(1000, token);
        }
        public virtual async Task RadioJammingFrsLoop(CancellationToken token)
        {
            await Task.Delay(3000, token);
        }
        public virtual async Task RadioJammingFhssLoop(CancellationToken token)
        {
            await Task.Delay(5000, token);
        }

        public virtual float[] SpectrumStorage { get; set; }
        public virtual List<byte> ByteSpectrumStorage { get; set; } = new(57000);

        public void InitSpectrumStorage(float MinFreqMHz, float MaxFreqMHz, int ResolutionHz)
        {
            this.MinFreqMHz = MinFreqMHz;
            this.MaxFreqMHz = MaxFreqMHz;
            this.ResolutionHz = ResolutionHz;

            int pointCount = Support.PointCount(MinFreqMHz, MaxFreqMHz, ResolutionHz);
            SpectrumStorage = new float[pointCount];
        }
        public void InitSpectrumStorage()
        {
            int pointCount = Support.PointCount(MinFreqMHz, MaxFreqMHz, ResolutionHz);
            SpectrumStorage = new float[pointCount];
        }

        public void InitKvetkaServerEvents()
        {
            KvetkaServer.ConnectCommandsIsNeeded += _KvetkaServer_ConnectCommandsIsNeeded;

            KvetkaServer.ObjectModeResponseIsNeeded += _KvetkaServer_ModeResponseIsNeeded;
            KvetkaServer.ObjectSpectrumResponseIsNeeded += _KvetkaServer_ObjectSpectrumResponseIsNeeded;

            KvetkaServer.ObjectAndIBinarySerializableResponseIsNeeded += _KvetkaServer_ObjectAndIBinarySerializableResponseIsNeeded;

            KvetkaServer.ObjectWaterfallResponseIsNeeded += _KvetkaServer_ObjectWaterfallResponseIsNeeded;

            KvetkaServer.ObjectDirFppResponceIsNeeded += KvetkaServer_ObjectDirFppResponceIsNeeded;
        }

        protected virtual async void KvetkaServer_ObjectDirFppResponceIsNeeded(KvetkaClientObject kvetkaClientObject, DirFppResponce dirFppRequest)
        {
            //some code for DirFpp

            //Add and Send some params to Generate DirFppResponce
            DirFppResponce dirFppResponce = new DirFppResponce();

            //Send DirFppResponce to Client
            _ = await KvetkaServer.ServerSendDirFppResponce(kvetkaClientObject, dirFppResponce);
        }

        public abstract byte[] GenerateSpectrum(int PointCount);

        public abstract Slice[] GenerateSlices(byte SlicesCount, int StepHz);


        public abstract FRSMessage GenerateFRSMessage(int N = 3);
        public abstract void Control(FRSMessage frsMessage);
        public abstract void Jamming(FRSMessage frsMessage);

        public abstract FHSSMessage GenerateFHSSMessage(int N = 4);
        public abstract void Control(FHSSMessage fhssMessage);
        public abstract void Jamming(FHSSMessage fhssMessage);
    }
}
