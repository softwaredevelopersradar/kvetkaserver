﻿using AWPtoDSPprotocolKvetka;
using KvetkaProtocol;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServerKvetkaClass
{
    public abstract partial class IServerController
    {
        private short Threshold { get; set; } = -80;

        private byte Attenuator { get; set; } = 16;
        private byte Amplifier { get; set; } = 23;

        private async void _KvetkaServer_ConnectCommandsIsNeeded(KvetkaClientObject kvetkaClientObject)
        {
            //3
            _ = await KvetkaServer.ServerSendFiltersMessage(kvetkaClientObject, new FiltersMessage(new MessageHeader(), Threshold));
            //4
            _ = await KvetkaServer.ServerSendSettingsMessage(kvetkaClientObject, new SettingsMessage(new MessageHeader(), Attenuator, Amplifier));
            //7
            _ = await KvetkaServer.ServerSendISGMessage(kvetkaClientObject, new ISGMessage(new MessageHeader(), 1));
        }

        private async void _KvetkaServer_ObjectAndIBinarySerializableResponseIsNeeded(KvetkaClientObject kvetkaClientObject, llcssKvetka.IBinarySerializable T)
        {
            switch (T)
            {
                case FiltersMessage filtersMessage:
                    Threshold = filtersMessage.Threshold;
                    _ = await KvetkaServer.ServerSendFiltersMessage(kvetkaClientObject, new FiltersMessage(new MessageHeader(), Threshold));
                    break;
                case SettingsMessage settingsMessage:
                    Attenuator = settingsMessage.Attenuator;
                    Amplifier = settingsMessage.Amplifier;
                    await Task.Delay(15);
                    _ = await KvetkaServer.ServerSendSettingsMessage(kvetkaClientObject, new SettingsMessage(new MessageHeader(), Attenuator, Amplifier));
                    break;
            }
        }

    }
}
