﻿using AWPtoDSPprotocolKvetka;
using KvetkaProtocol;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;


namespace ServerKvetkaClass
{
    public abstract partial class IServerController
    {
        protected Task MainTask;
        protected CancellationTokenSource ctsCurrentServerMode;
        protected CancellationToken tokenCurrentServerMode;

        protected KvetkaServerMode CurrentServerMode { get; set; } = KvetkaServerMode.Stop;


        private async void _KvetkaServer_ModeResponseIsNeeded(KvetkaClientObject kvetkaClientObject, ModeMessage modeMessage)
        {
            KvetkaServerMode currentServerMode = await Switcher(modeMessage.Mode);

            CurrentServerMode = currentServerMode;

            modeMessage = GenerateModeMessage(currentServerMode);

            _ = await KvetkaServer.ServerSendModeMessage(kvetkaClientObject, modeMessage);
        }

        private static ModeMessage GenerateModeMessage(KvetkaServerMode kvetkaServerMode, byte ErrorCode = 0)
        {
            return new ModeMessage()
            {
                Header = new MessageHeader()
                {
                    ErrorCode = ErrorCode,
                    InformationLength = ModeMessage.BinarySize - MessageHeader.BinarySize
                },
                Mode = kvetkaServerMode
            };
        }

        protected TaskCompletionSource<KvetkaServerMode> tcsModeAwaiter;

        protected virtual async Task<KvetkaServerMode> Switcher(KvetkaServerMode RequestServerMode)
        {
            if (MainTask != null)
            {
                ctsCurrentServerMode?.Cancel();
                try
                {
                    await MainTask;
                    //Console.WriteLine("Success");
                }
                catch (OperationCanceledException)
                {
                    Console.WriteLine("Cancelled");
                    CurrentServerMode = KvetkaServerMode.Stop;
                }
                catch (Exception e)
                {
                    Console.WriteLine("Error: {0}", e);
                    CurrentServerMode = KvetkaServerMode.Stop;
                }
            }

            ctsCurrentServerMode = new CancellationTokenSource();
            tokenCurrentServerMode = ctsCurrentServerMode.Token;

            if (CurrentServerMode != RequestServerMode)
            {
                tcsModeAwaiter = new TaskCompletionSource<KvetkaServerMode>();

                RequestServerMode = await ModeSwitch(RequestServerMode, tokenCurrentServerMode);

                switch (RequestServerMode)
                {
                    case KvetkaServerMode.Stop:
                        break;
                    case KvetkaServerMode.RadioIntelligence:
                        MainTask = Task.Run(() => RadioIntelligence(RequestServerMode, tokenCurrentServerMode));
                        break;
                    case KvetkaServerMode.RadioJammingFrs:
                        MainTask = Task.Run(() => RadioJammingFrs(RequestServerMode, tokenCurrentServerMode));
                        break;
                    case KvetkaServerMode.RadioJammingFhss:
                        MainTask = Task.Run(() => RadioJammingFhss(RequestServerMode, tokenCurrentServerMode));
                        break;
                }

                return RequestServerMode;

                //return CurrentServerMode = await tcsModeAwaiter.Task;
            }
            else
            {
                return RequestServerMode;
            }
        }


        private async Task RadioIntelligence(KvetkaServerMode kvetkaServerMode, CancellationToken token)
        {
            Console.WriteLine($"{kvetkaServerMode} Start");
            while (true)
            {
                if (token.IsCancellationRequested)
                {
                    Console.WriteLine($"TestLoop {kvetkaServerMode} прерван токеном");
                    return;
                }

                if (kvetkaServerMode == KvetkaServerMode.RadioIntelligence && SpectrumLoop)
                {
                    byte[] SpectrumToResponce = GenerateSpectrum(SpectrumPointCountLoop);

                    SpectrumResponse spectrumResponse = GenerateSpectrumResponce(SpectrumToResponce);

                    _ = await KvetkaServer.ServerSendAllSpectrumResponse(spectrumResponse);
                }

                if (kvetkaServerMode == KvetkaServerMode.RadioIntelligence && WaterfallLoop)
                {
                    Slice[] Slices = GenerateSlices(WaterfallSlicesCountLoop, WaterfallStepHzLoop);

                    WaterfallResponse waterfallResponse = GenerateWaterfallResponse(Slices);

                    _ = await KvetkaServer.ServerSendAllWaterfallResponse(waterfallResponse);
                }

                await Task.Delay(16, token);
            }
        }

        private async Task RadioJammingFrs(KvetkaServerMode kvetkaServerMode, CancellationToken token)
        {
            Console.WriteLine($"{kvetkaServerMode} Start");

            FRSMessage frsMessage = GenerateFRSMessage();

            while (true)
            {
                if (token.IsCancellationRequested)
                {
                    Console.WriteLine($"TestLoop {kvetkaServerMode} прерван токеном");
                    return;
                }

                //Control
                Control(frsMessage);

                await KvetkaServer.ServerSendAllFRSMessage(frsMessage);

                await Task.Delay(500, token);

                //Jamming
                Jamming(frsMessage);

                await KvetkaServer.ServerSendAllFRSMessage(frsMessage);

                await Task.Delay(500, token);
            }
        }

        private async Task RadioJammingFhss(KvetkaServerMode kvetkaServerMode, CancellationToken token)
        {
            Console.WriteLine($"{kvetkaServerMode} Start");

            FHSSMessage fhssMessage = GenerateFHSSMessage();

            while (true)
            {
                if (token.IsCancellationRequested)
                {
                    Console.WriteLine($"TestLoop {kvetkaServerMode} прерван токеном");
                    return;
                }

                //Control
                Control(fhssMessage);

                await KvetkaServer.ServerSendAllFHSSMessage(fhssMessage);

                await Task.Delay(500, token);

                //Jamming

                Jamming(fhssMessage);

                await KvetkaServer.ServerSendAllFHSSMessage(fhssMessage);

                await Task.Delay(500, token);
            }
        }

        //tcsModeAwaiter.TrySetResult(KvetkaServerMode.Stop);
    }
}
