﻿using AWPtoDSPprotocolKvetka;
using KvetkaProtocol;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServerKvetkaClass
{
    public abstract partial class IServerController
    {

        private bool WaterfallLoop = false;

        private byte WaterfallSlicesCountLoop = 1;

        private int WaterfallStepHzLoop = 40000;

        private async void _KvetkaServer_ObjectWaterfallResponseIsNeeded(KvetkaClientObject kvetkaClientObject, WaterfallRequest waterfallRequest)
        {
            Slice[] Slices = GenerateSlices(waterfallRequest.SlicesCount, waterfallRequest.StepHz);

            WaterfallResponse waterfallResponse = GenerateWaterfallResponse(Slices);

            WaterfallLoopConverter(waterfallRequest);

            _ = await KvetkaServer.ServerSendWaterfallResponse(kvetkaClientObject, waterfallResponse);
        }

        private void WaterfallLoopConverter(WaterfallRequest waterfallRequest)
        {
            if (waterfallRequest.Mode == 0)
            {
                WaterfallLoop = false;
            }
            if (waterfallRequest.Mode == 2)
            {
                WaterfallSlicesCountLoop = waterfallRequest.SlicesCount;
                WaterfallStepHzLoop = waterfallRequest.StepHz;
                WaterfallLoop = true;
            }
        }

        private WaterfallResponse GenerateWaterfallResponse(Slice[] Slices)
        {
            int InformationLength = 1 + GetSlicesInformationLength(Slices);

            MessageHeader header = new MessageHeader(0, 0, 0, 0, InformationLength);
            WaterfallResponse waterfallResponse = new WaterfallResponse(header, (byte)Slices.Length, Slices);

            return waterfallResponse;
        }

        private int GetSlicesInformationLength(IEnumerable<Slice> Slices)
        {
            int InformationLength = 0;

            foreach (Slice slice in Slices)
            {
                InformationLength += slice.StructureBinarySize;
            }

            return InformationLength;
        }
    }
}
