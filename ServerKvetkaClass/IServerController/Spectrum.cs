﻿using AWPtoDSPprotocolKvetka;
using KvetkaProtocol;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace ServerKvetkaClass
{
    public abstract partial class IServerController
    {
        int ResolutionHz = 100;

        private bool SpectrumLoop { get; set; } = false;
        private int SpectrumPointCountLoop { get; set; } = 3000;


        protected virtual async void _KvetkaServer_ObjectSpectrumResponseIsNeeded(KvetkaClientObject kvetkaClientObject, SpectrumRequest spectrumRequest)
        {
            //SpectrumStorage = GenerateFloatSpectrum(SpectrumStorage.Length);

            //Ужатие Спектра от Феди
            //var segment = new ArraySegment<float>(SpectrumStorage, 0, SpectrumStorage.Length);
            //var spectrum = segment.StrechSpectrum(PointCount(spectrumRequest.StepHz));
            //byte[] SpectrumToResponce = spectrum.Amplitudes.Select(x=> (byte)x).ToArray();

            byte[] SpectrumToResponce = GenerateSpectrum(Support.PointCount(MinFreqMHz, MaxFreqMHz, spectrumRequest.StepHz));

            SpectrumResponse spectrumResponse = GenerateSpectrumResponce(SpectrumToResponce);

            SpectrumLoopConverter(spectrumRequest);

            _ = await KvetkaServer.ServerSendSpectrumResponse(kvetkaClientObject, spectrumResponse);
        }

        protected void SpectrumLoopConverter(SpectrumRequest spectrumRequest)
        {
            if (spectrumRequest.Mode == 0)
            {
                SpectrumLoop = false;
            }
            if (spectrumRequest.Mode == 2)
            {
                SpectrumPointCountLoop = Support.PointCount(MinFreqMHz, MaxFreqMHz, spectrumRequest.StepHz);
                SpectrumLoop = true;
            }
        }

        protected SpectrumResponse GenerateSpectrumResponce(byte[] Spectrum, byte ErrorCode = 0)
        {
            return new SpectrumResponse()
            {
                Header = new MessageHeader()
                {
                    ErrorCode = ErrorCode,
                    InformationLength = Spectrum.Length
                },
                Spectrum = Spectrum
            };
        }
    }
}
