﻿using AWPtoDSPprotocolKvetka;
using KvetkaProtocol;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BoraClientDll;
using YamlWorker;
using Pelengator_DLL;
using VereskClientDll;

namespace ServerKvetkaClass
{
    public class ServerKvetkaMainClass
    {
        private KvetkaServer KvetkaServer = new();
        private DbClient dbClient;
        private BoraClient _boraClient;
        private PelengatorTcp _pelengatorClient;
        private VereskClientTcp _vereskClient;

        private IServerController ServerController;

        private Settings _settings = new()
        {
            workMode = WorkMode.Work,
            ServerSettings = new EndPointConnection()
            {
                IpAddress = "127.0.0.1",
                Port = 20003
            },
            DBSettings = new EndPointConnection()
            {
                IpAddress = "127.0.0.1",
                Port = 30051
            },
            BoraSettings = new EndPointConnection()
            {
                IpAddress = "192.168.0.107",
                Port = 6789
            },
            PelengatorSettings = new EndPointConnection()
            {
                IpAddress = "192.168.0.105",
                Port = 7777
            },
            VereskSettings = new EndPointConnection()
            {
                IpAddress = "86.57.245.94",
                Port = 32133
            }
        };
        protected internal Settings Settings
        {
            get => _settings;
            set
            {
                if (value != null && value != _settings)
                {
                    _settings = value;
                }
            }
        }

        public ServerKvetkaMainClass()
        {

        }


        public async void Start()
        {
            //Yaml.YamlSave(settings, "Settings.yaml");
            Settings = Yaml.YamlLoad<Settings>("Settings.yaml");

            dbClient = new DbClient(Settings.DBSettings.IpAddress, Settings.DBSettings.Port);
            _boraClient = new BoraClient();
            _pelengatorClient = new PelengatorTcp(Settings.PelengatorSettings.IpAddress, Settings.PelengatorSettings.Port);
            _vereskClient = new VereskClientTcp(Settings.VereskSettings.IpAddress, Settings.VereskSettings.Port);


            //string connectToBora = await _boraClient.IQConnectToServer(Settings.BoraSettings.IpAddress, Settings.BoraSettings.Port);

            ServerController = Settings.workMode switch
            {
                WorkMode.Simulate => new ServerKvetkaSimulate(KvetkaServer),
                WorkMode.Work => new ServerKvetkaWork(KvetkaServer, _boraClient, dbClient, _pelengatorClient, _vereskClient),
                _ => throw new NotImplementedException()
            };

            ServerController.InitKvetkaServerEvents();

            ServerController.InitSpectrumStorage();

            dbClient.Connect();

            await KvetkaServer.ServerStart(Settings.ServerSettings.IpAddress, Settings.ServerSettings.Port);
        }


    }
}
