﻿using AWPtoDSPprotocolKvetka;
using KvetkaModelsDBLib;
using KvetkaProtocol;
using Pelengator_DLL;
using System;
using System.Threading.Tasks;

namespace ServerKvetkaClass
{
    public partial class ServerKvetkaWork : IServerController
    {
        /// <summary>
        /// Связь с пеленгатором
        /// </summary>
        private bool PelengatorIsConnected { get; set; }

        /// <summary>
        /// Имя регистрируемого поста
        /// </summary>
        private string SenderName { get; set; } = "RADAR01";

        /// <summary>
        /// Имя регистрирующего поста (Из Ответа на «регистрацию»)
        /// </summary>
        private string DestName { get; set; } 

        /// <summary>
        /// Подписываемся на события
        /// </summary>
        public void SubscribeToPelengatorEvents()
        {
            _pelengatorClient.OnConnect += _pelengatorClient_OnConnect;
            _pelengatorClient.OnDisconnect += _pelengatorClient_OnDisconnect;
            _pelengatorClient.OnPeleng += _pelengatorClient_OnPeleng;
            _pelengatorClient.OnEof += _pelengatorClient_OnEof;
        }

        /// <summary>
        /// Отписываемся от событий
        /// </summary>
        public void UnsubscribeFromPelengatorEvents()
        {
            _pelengatorClient.OnConnect -= _pelengatorClient_OnConnect;
            _pelengatorClient.OnDisconnect -= _pelengatorClient_OnDisconnect;
            _pelengatorClient.OnPeleng -= _pelengatorClient_OnPeleng;
            _pelengatorClient.OnEof -= _pelengatorClient_OnEof;
        }

        /// <summary>
        /// Запрос «регистрации»
        /// </summary>
        /// <param name="password">Пароль</param>
        public async void Registration(string password) //TODO: Вписать параметры
        {
            if (!PelengatorIsConnected)
                return;

            var registration = await _pelengatorClient.Registration(SenderName, password);

            if (registration == null)
                return;

            DestName = registration.UnpRegister.Hostname;
        }

        /// <summary>
        /// Запрос ЧПП
        /// </summary>
        public async Task<Fppresult> FppRequest() //TODO: Вписать параметры, отправить ответ в базу
        {
            if (!PelengatorIsConnected)
                return null;

            var fppRequest = await _pelengatorClient.FppRequest(SenderName, DestName);

            if (fppRequest == null)
                return null;

            return fppRequest;
        }

        /// <summary>
        /// Команда на пеленгование 
        /// </summary>
        /// <param name="freqHz">Частота (Гц) (100 000 – 30 000 000)</param>
        /// <param name="bandWidth">Полоса (Гц) (300 – 12 000)</param>
        /// <param name="filter">Частотное разрешение по частоте (Гц)(10,100,1000 Гц).</param>
        public async void DelayedBearing(long freqHz, int bandWidth, short filter)
        {
            if (!PelengatorIsConnected)
                return;

            var delayedBearing = await _pelengatorClient.DelayedBearing(SenderName, DestName, freqHz, bandWidth, filter);

            if (delayedBearing == null)
                return;

        }

        #region EventHandlers
        protected override async void KvetkaServer_ObjectDirFppResponceIsNeeded(KvetkaClientObject kvetkaClientObject, DirFppResponce dirFppRequest)
        {
            var fppResponce = FppRequest().Result;

            var dirFppResponce = GetFppResponce(fppResponce);

            _ = await KvetkaServer.ServerSendDirFppResponce(kvetkaClientObject, dirFppResponce);
        }

        /// <summary>
        /// Событие на отключение от пеленгатора
        /// </summary>
        /// <param name="socket">Сокет</param>
        private void _pelengatorClient_OnDisconnect(object sender, string socket)
        {
            PelengatorIsConnected = false;
            Console.WriteLine("Disconnected from Pelengator:" + socket);
            UnsubscribeFromPelengatorEvents();
        }

        /// <summary>
        /// Событие на подключение к пеленгатору
        /// </summary>
        /// <param name="socket">Сокет</param>
        private void _pelengatorClient_OnConnect(object sender, string socket)
        {
            PelengatorIsConnected = true;
            Console.WriteLine("Connected to Pelengator: " + socket);
        }

        /// <summary>
        /// Результат пеленгования
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="pelengResult">Результат</param>
        private void _pelengatorClient_OnPeleng(object sender, PelengResult pelengResult)
        {
            var recordDb = new TableResFF
            {
                FrequencyKHz = pelengResult.Freq / 1000,
                Coordinates = new Coord() { Latitude = pelengResult.Post.Lat, Longitude = pelengResult.Post.Lon },
                Time = pelengResult.Time,
                BearingOwn = (float)pelengResult.HDir,
                Level = (short)pelengResult.Pwr,
                DistanceOwn = (float)pelengResult.Distance
            };

            _dbClient?.AddRecord(NameTable.TableResFF, recordDb);
        }

        /// <summary>
        /// Завершение пеленгования
        /// </summary>
        /// <param name="endOfPeleng">Результат</param>
        private void _pelengatorClient_OnEof(object sender, PelengEoF endOfPeleng) //TODO: Что делать после окончания пеленга?
        {

        }
        #endregion

        private DirFppResponce GetFppResponce(Fppresult fppResponce)
        {
            if(fppResponce == null)
                return new DirFppResponce()
                {
                    Header = new MessageHeader(0, 0, 0, 0, KvetkaProtocol.DirFpp.NullValueBinarySize),
                };

            var dirFpp = new KvetkaProtocol.DirFpp()
            {
                BFreq = fppResponce.Dir.BFreq,
                EFreq = fppResponce.Dir.EFreq,
                Id = fppResponce.Dir.Id,
                Step = fppResponce.Dir.Step,
                N = fppResponce.Dir.N,
                AzsArr = fppResponce.Dir.AzsArr,
                LevelsArr = fppResponce.Dir.LevelsArr
            };

            var dirFppResponce = new DirFppResponce()
            {
                dirFpp = dirFpp,
                Header = new MessageHeader()
                {
                    ErrorCode = 0,
                    InformationLength = dirFpp.GetBytes().Length
                }
            };

            return dirFppResponce;
        }
    }
}
