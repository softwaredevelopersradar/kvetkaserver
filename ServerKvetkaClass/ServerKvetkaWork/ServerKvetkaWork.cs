﻿using KvetkaProtocol;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AWPtoDSPprotocolKvetka;
using BoraClientDll;
using Pelengator_DLL;
using DataProcessorSpace;
using DspDataModel;
using DspDataModel.DataProcessor;
using YamlWorker;
using VereskClientDll;


namespace ServerKvetkaClass
{
    public partial class ServerKvetkaWork : IServerController
    {
        private readonly BoraClient _boraClient;

        private readonly DbClient _dbClient;

        private readonly PelengatorTcp _pelengatorClient;

        private readonly VereskClientTcp _vereskClient;

        public static Config config = new Config();

        public ServerKvetkaWork(KvetkaServer kvetkaServer, BoraClient boraClient, DbClient dbClient, PelengatorTcp pelengatorClient, VereskClientTcp vereskClient) : base(kvetkaServer)
        {
            _boraClient = boraClient;

            _dbClient = dbClient;

            _pelengatorClient = pelengatorClient;

            _vereskClient = vereskClient;

            if (_vereskClient != null)
            {
                SubscribeToVereskEvents();
                var vereskConnectionResult = _vereskClient.Connect().Result;
            }

            if (_pelengatorClient != null)
            {
                SubscribeToPelengatorEvents();
                var connectToPelengator = _pelengatorClient.Connect().Result;
                Registration("RADAR");
            }

            config = Yaml.YamlLoad<Config>(Config.ConfigPath);

            dataProcessor0 = new DataProcessor(config);
        }

        protected override async Task<KvetkaServerMode> Switcher(KvetkaServerMode RequestServerMode)
        {
            if (MainTask != null)
            {
                ctsCurrentServerMode?.Cancel();
                try
                {
                    await MainTask;
                    //Console.WriteLine("Success");
                }
                catch (OperationCanceledException)
                {
                    Console.WriteLine("Cancelled");
                    CurrentServerMode = KvetkaServerMode.Stop;
                }
                catch (Exception e)
                {
                    Console.WriteLine("Error: {0}", e);
                    CurrentServerMode = KvetkaServerMode.Stop;
                }
            }

            ctsCurrentServerMode = new CancellationTokenSource();
            tokenCurrentServerMode = ctsCurrentServerMode.Token;

            if (CurrentServerMode != RequestServerMode)
            {
                tcsModeAwaiter = new TaskCompletionSource<KvetkaServerMode>();

                switch (RequestServerMode)
                {
                    case KvetkaServerMode.Stop:
                        break;
                    case KvetkaServerMode.RadioIntelligence:
                        MainTask = Task.Run(() => RadioIntelligenceLoop(tokenCurrentServerMode));
                        break;
                    case KvetkaServerMode.RadioJammingFrs:
                        MainTask = Task.Run(() => RadioJammingFrsLoop( tokenCurrentServerMode));
                        break;
                    case KvetkaServerMode.RadioJammingFhss:
                        MainTask = Task.Run(() => RadioJammingFhssLoop(tokenCurrentServerMode));
                        break;
                }

                return RequestServerMode;

                //return CurrentServerMode = await tcsModeAwaiter.Task;
            }
            else
            {
                return RequestServerMode;
            }
        }

        public new async Task RadioIntelligenceLoop(CancellationToken token)
        {
            double centralFreqHz = 15.75 * 1e6;
            double bandwidthHz = 14.25 * 1e6;

            while (!token.IsCancellationRequested)
            {
                //Запросить спектр у Боры
                var answer = await _boraClient.GetSpectrum(centralFreqHz, bandwidthHz, BoraProtocol.FilterSpectrum.filter_1000);

                //Запросить спектр у Боры
                //var answer1 = await _boraClient.GetSpectrum(centralFreqHz, bandwidthHz, BoraProtocol.FilterSpectrum.filter_500);

                //Запросить спектр у Боры
                //var answer2 = await _boraClient.GetSpectrum(centralFreqHz, bandwidthHz, BoraProtocol.FilterSpectrum.filter_100);

                //Получить результат спектра
                var spectrumResult = answer?.spectrum?.powers.Select(x => x * -1.0f).ToArray();
                if (spectrumResult != null)
                {
                    //Записать в хранилище
                    base.SpectrumStorage = spectrumResult;

                    //Обнаружение ИРИ от Феди
                    ProcessResult result0 = FedyasWorkPlusSignalTimePlusFilters(SpectrumStorage, 0, 0, 0);

                    //Обнаружение Сигналов
                    TableSignalsWork(result0);

                    //Записать в байтовое хранилище
                    base.ByteSpectrumStorage = new List<byte>(answer.spectrum.powers);
                }

                await Task.Delay(1);
            }
        }

        protected override async void _KvetkaServer_ObjectSpectrumResponseIsNeeded(KvetkaClientObject kvetkaClientObject, SpectrumRequest spectrumRequest)
        {
            //SpectrumStorage = GenerateFloatSpectrum(SpectrumStorage.Length);

            //Ужатие Спектра от Феди
            //var segment = new ArraySegment<float>(SpectrumStorage, 0, SpectrumStorage.Length);
            //var spectrum = segment.StrechSpectrum(PointCount(spectrumRequest.StepHz));
            //byte[] SpectrumToResponce = spectrum.Amplitudes.Select(x=> (byte)x).ToArray();

            //byte[] SpectrumToResponce = GenerateSpectrum(Support.PointCount(MinFreqMHz, MaxFreqMHz, spectrumRequest.StepHz));

            SpectrumResponse spectrumResponse = SpectrumResponse(spectrumRequest);

            _ = await KvetkaServer.ServerSendSpectrumResponse(kvetkaClientObject, spectrumResponse);
        }

        private SpectrumResponse SpectrumResponse(SpectrumRequest spectrumRequest)
        {
            SpectrumResponse spectrumResponse;
            if (CheckSpectrumRequest(spectrumRequest))
            {
                double minFreqkHz = spectrumRequest.StartFrequencykHzX10 / 10d;
                double maxFreqkHz = spectrumRequest.EndFrequencykHzX10 / 10d;

                byte[] SpectrumToResponce = GenerateByteSpectrum(minFreqkHz, maxFreqkHz);

                spectrumResponse = GenerateSpectrumResponce(SpectrumToResponce);

                SpectrumLoopConverter(spectrumRequest);
            }
            else
            {
                spectrumResponse = GenerateSpectrumResponce(new byte[0], 1);
            }

            return spectrumResponse;
        }

        private bool CheckSpectrumRequest(SpectrumRequest spectrumRequest)
        {
            if (spectrumRequest.StartFrequencykHzX10 > spectrumRequest.EndFrequencykHzX10) return false;

            if (spectrumRequest.StartFrequencykHzX10 / 1e4 < MinFreqMHz || spectrumRequest.StartFrequencykHzX10 / 1e4 > MaxFreqMHz) return false;
            if (spectrumRequest.EndFrequencykHzX10 / 1e4 < MinFreqMHz || spectrumRequest.EndFrequencykHzX10 / 1e4 > MaxFreqMHz) return false;

            return true;
        }

        public new async Task RadioJammingFrsLoop(CancellationToken token)
        {
            await Task.Delay(3000, token);
        }
        public new async Task RadioJammingFhssLoop(CancellationToken token)
        {
            await Task.Delay(5000, token);
        }

        public override async Task<KvetkaServerMode> ModeSwitch(KvetkaServerMode requestServerMode, CancellationToken token)
        {
            switch (requestServerMode)
            {
                case KvetkaServerMode.Stop:
                    break;
                case KvetkaServerMode.RadioIntelligence:
                    await RadioIntelligenceLoop(token);
                    break;
                case KvetkaServerMode.RadioJammingFrs:
                    await RadioJammingFrsLoop(token);
                    break;
                case KvetkaServerMode.RadioJammingFhss:
                    await RadioJammingFhssLoop(token);
                    break;
                default:
                    break;
            }
            return requestServerMode;
        }

        public override byte[] GenerateSpectrum(int count = 3000)
        {
            Random random = new();

            byte[] spectrum = new byte[count];

            return spectrum.Select(x => x = (byte)(80 + random.Next(0, 20))).ToArray();
        }

        public byte[] GenerateByteSpectrum(double minFreqkHz, double maxFreqkHz)
        {
            double globalDifferenceMhz = MaxFreqMHz - MinFreqMHz;

            double resolutionkHz = (globalDifferenceMhz / ByteSpectrumStorage.Count) * 1e3;

            double localDifferencekHz = maxFreqkHz - minFreqkHz;

            int localDifferencePoints = (int)(localDifferencekHz * resolutionkHz);

            double localDifferenceStartkHz = minFreqkHz - MinFreqMHz * 1e3;

            int localDifferenceStartPoints = (int)(localDifferenceStartkHz * resolutionkHz);

            return ByteSpectrumStorage.GetRange(localDifferenceStartPoints, localDifferencePoints).ToArray();
        }

        public override Slice[] GenerateSlices(byte slicesCount, int stepHz)
        {
            int AmplitudesCount = Support.PointCount(MinFreqMHz, MaxFreqMHz, stepHz);

            Slice[] Slices = new Slice[slicesCount];

            for (int i = 0; i < slicesCount; i++)
            {
                Slices[i] = new Slice()
                {
                    Count = AmplitudesCount,
                    Amplitudes = GenerateApmlitudes(AmplitudesCount),
                };
            }

            return Slices;
        }

        private byte[] GenerateApmlitudes(int amplitudesCount)
        {
            Random random = new Random();

            byte[] Amplitudes = new byte[amplitudesCount];

            Amplitudes = Amplitudes.Select(x => x = (byte)(random.Next(0, 120))).ToArray();

            return Amplitudes;
        }

        public override FRSMessage GenerateFRSMessage(int N = 3)
        {
            throw new NotImplementedException();
        }
        public override void Control(FRSMessage frsMessage)
        {
            throw new NotImplementedException();
        }
        public override void Jamming(FRSMessage frsMessage)
        {
            throw new NotImplementedException();
        }

        public override FHSSMessage GenerateFHSSMessage(int N = 4)
        {
            throw new NotImplementedException();
        }
        public override void Control(FHSSMessage fhssMessage)
        {
            throw new NotImplementedException();
        }
        public override void Jamming(FHSSMessage fhssMessage)
        {
            throw new NotImplementedException();
        }

    }
}