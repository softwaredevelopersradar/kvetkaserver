﻿using KvetkaModelsDBLib;
using Rdmp.Veresk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VereskClientDll;


namespace ServerKvetkaClass
{
    public partial class ServerKvetkaWork : IServerController
    {
        public bool VereskIsConnected { get; private set; }
        private VereskResult lastVereskResultInfo;

        #region Commands

        public void SubscribeToVereskEvents()
        {
            _vereskClient.OnConnect += _client_OnConnect;
            _vereskClient.OnDisconnect += _client_OnDisconnect;
            //_vereskClient.OnWriteBytes += _client_OnWriteBytes;
            //_vereskClient.OnReadBytes += _client_OnReadBytes;
            _vereskClient.OnGetVereskEvent += _client_OnGetVereskEvent;
            _vereskClient.OnGetVereskFileFragment += _client_OnGetVereskFileFragment; ;
            _vereskClient.OnGetVereskNotification += _client_OnGetVereskNotification; ;
            _vereskClient.OnGetVereskResult += _client_OnGetVereskResult;
            _vereskClient.OnGetOperationsInstructionsReport += _client_OnGetOperationsInstructionsReport;
        }

        public void UnsubscribeFromVereskEvents()
        {
            _vereskClient.OnConnect -= _client_OnConnect;
            _vereskClient.OnDisconnect -= _client_OnDisconnect;
            //_vereskClient.OnWriteBytes -= _client_OnWriteBytes;
            //_vereskClient.OnReadBytes -= _client_OnReadBytes;
            _vereskClient.OnGetVereskFileFragment -= _client_OnGetVereskFileFragment; ;
            _vereskClient.OnGetVereskNotification -= _client_OnGetVereskNotification; ;
            _vereskClient.OnGetVereskResult -= _client_OnGetVereskResult;
            _vereskClient.OnGetOperationsInstructionsReport -= _client_OnGetOperationsInstructionsReport;
        }

        public void SendTaskToVeresk()
        {
            if (!VereskIsConnected)
                return;

            var answer = _vereskClient.AddTask();
            if (answer == null)
            { Console.WriteLine("No reply from VERESK!"); }
        }

        public void SendTaskToVereskWithParams()
        {
            if (!VereskIsConnected)
                return;
            var signals = new List<SignalClass>() {
                SignalClass.ScSt4285,
                SignalClass.ScSt4529,
                SignalClass.ScMil110A,
                SignalClass.ScMil110BAppc,
                SignalClass.ScMil110CAppd,
                SignalClass.ScM39,
                SignalClass.ScAle2G,
                SignalClass.ScAle3G,
                SignalClass.ScRs,
                SignalClass.ScClover, 
                SignalClass.ScCodan, 
                SignalClass.ScDc7200,
                SignalClass.ScLink22,
                SignalClass.ScB162, 
                SignalClass.ScHfdl,
                SignalClass.ScAselsan
            };

            var freqRanges = new List<FreqRange>() { new FreqRange() { StartFreq = 1500000, EndFreq = 30000000, Delta = 2500 }}; //TODO: вписать частоты с бд
            var freqs = new List<uint>() { 9795000, 7845000 }; //TODO: вписать частоты с бд
            var answer = _vereskClient.AddTask(signals, freqRanges, freqs).Result;
            if (answer == null)
            { Console.WriteLine("No reply from VERESK!"); }
            //else
            //{ } (Rdmp.OperationsInstructionsReport.Types.Type).Type;
        }

        public void DeleteTask()
        {
            if (!VereskIsConnected)
                return;

            var answer = _vereskClient.DeleteTask().Result;
            if (answer == null)
            { Console.WriteLine("No reply from VERESK!"); }
            
            //(Rdmp.OperationsInstructionsReport.Types.Type).Type
        }
        
        #endregion





        #region EventHandlers
        private void _client_OnDisconnect(object sender, EventArgs e)
        {
            VereskIsConnected = false;
            Console.WriteLine("Disconnected from VERESK");
            UnsubscribeFromVereskEvents();
        }

        private void _client_OnConnect(object sender, EventArgs e)
        {
            VereskIsConnected = true;
            Console.WriteLine("Connected to VERESK: " + _vereskClient.IpAddress + ":" + _vereskClient.Port);
        }

        private void _client_OnGetOperationsInstructionsReport(object sender, Rdmp.OperationsInstructionsReport e)
        {
           Console.WriteLine("Got VERESK reply on command, result = " + (Rdmp.OperationsInstructionsReport.Types.Type)e.Type + ", description: " + e.Description);
        }

        private void _client_OnGetVereskResult(object sender, VereskResult e)
        {
            Console.WriteLine("Got VERESK result of the task , freq = " + e.ProcSessionInfo.Freq + ", network =" + e.ProcSessionInfo.RadioNet.Id + ":" + e.ProcSessionInfo.RadioNet.Name + ", recvrChannel =" + e.ProcSessionInfo.RecvrChannel ); //+ ", from =" + e.ProcSessionInfo.FromList.FirstOrDefault().
            //WriteResToDB(e);
            lastVereskResultInfo = e.Clone();
            DelayedBearing(e.ProcSessionInfo.Freq, 6000, 100);
        }

        private void _client_OnGetVereskNotification(object sender, EventArgs e)
        {
            //OnMessToMain(sender, "Got Notification");
        }

        private void _client_OnGetVereskFileFragment(object sender, Rdmp.FileFragment e)
        {
            //OnMessToMain(sender, "Got FileFragment, name = " + e.Filename);
        }

        private void _client_OnGetVereskEvent(object sender, VereskEvent e)
        {
            switch (e.EventType)
            {
                case VereskEventType.VetOpenedSession:
                    Console.WriteLine("VERESK opened session, freq = " + e.OpenSess?.DetectFreq + ", signal class = " + e.OpenSess.SigClass);
                    //WriteResToDb(e.OpenSess);
                    break;
                case VereskEventType.VetClosedSession:
                    Console.WriteLine("VERESK closed session, freq = " + e.CloseSess?.SessFreq + ", signal class = " + e.CloseSess.SigClassesList.FirstOrDefault());
                    break;
                case VereskEventType.VetSignalDetected:
                    Console.WriteLine("VERESK detected signal, freq = " + e.SigDet?.DetectFreq + ", signal class = " + e.SigDet.SigClass);
                    break;
                default:
                    break;
            }

        }
        #endregion

        #region ToDb
        private void WriteResToDb(VereskOpenedSession data)
        {
            var recordDb = new TableResFF();
            recordDb.FrequencyKHz = data.DetectFreq / 1000;
            recordDb.Control = Led.Empty;
            //recordDb.Priority = 0;
            //recordDb.RadioNetwork = 0;
            recordDb.Type = (byte)data.SigClass.SignalClass;
            recordDb.Time = DateTimeOffset.FromUnixTimeMilliseconds((long)data.StartTimestamp).DateTime;

            _dbClient?.AddRecord(NameTable.TableResFF, recordDb);
        }

        private void WriteResToDB(VereskResult data)

        {
            var recordDb = new TableResFF();
            recordDb.FrequencyKHz = data.ProcSessionInfo.Freq / 1000;
            recordDb.Control = Led.Empty;
            recordDb.Type = (byte)data.ProcSessionInfo.SigClassesList.FirstOrDefault().SignalClass;
            recordDb.Time = DateTimeOffset.FromUnixTimeMilliseconds((long)data.ProcSessionInfo.StartTimestamp).DateTime;
            recordDb.RadioNetwork = 1;

            _dbClient?.AddRecord(NameTable.TableResFF, recordDb);
        }
        #endregion
    }
}
