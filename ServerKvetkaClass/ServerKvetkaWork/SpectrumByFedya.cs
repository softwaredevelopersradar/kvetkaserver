﻿using KvetkaProtocol;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AWPtoDSPprotocolKvetka;
using BoraClientDll;
using DspDataModel;
using DspDataModel.DataProcessor;
using KvetkaModelsDBLib;


namespace ServerKvetkaClass
{
    public partial class ServerKvetkaWork : IServerController
    {
        DataProcessorSpace.DataProcessor dataProcessor0;

        Dictionary<ISignal, DateTime> SignalTime = new Dictionary<ISignal, DateTime>();
        private ProcessResult FedyasWorkPlusSignalTimePlusFilters(IEnumerable<float> Ampl, int EPO,
            float _FilterMinBandWidthkHz, float _FilterMaxBandWidthkHz, float threshold = -80)
        {
            //Обнаружение ИРИ от Феди
            DspDataModel.Data.IAmplitudeScan amplitudeScan0 = new DspDataModel.Data.AmplitudeScan(Ampl.ToArray(), EPO, DateTime.Now, 0);

            //Параметр для поиска сигналов по порогу
            ScanProcessConfig scanProcessConfig = ScanProcessConfig.CreateConfigWithoutDf(threshold);

            //Результаты сигналов
            var result0 = dataProcessor0.GetSignals(amplitudeScan0, scanProcessConfig);

            //Ассоциация сигнал-время
            if (result0.RawSignals.Count != 0 || result0.Signals.Count != 0)
            {
                //Console.WriteLine(result0.Signals.Count);
                SignalTime.Clear();
                for (int qq = 0; qq < result0.Signals.Count; qq++)
                {
                    SignalTime.Add(result0.Signals[qq], DateTime.Now);
                }
            }

            //Фильтр по диапазонам РР
            bool RR_FILTER = false;
            if (RR_FILTER)
            {
                var signals = new List<ISignal>();

                for (int v = 0; v < result0.Signals.Count; v++)
                {
                    if (FRSonRangeRI(result0.Signals[v].CentralFrequencyKhz / 1000d))
                    {
                        signals.Add(result0.Signals[v]);
                    }
                }
                result0 = new ProcessResult(signals);
            }

            //Фильтр по ширине
            bool BANDWIDTH_FILTER = false;
            if (BANDWIDTH_FILTER)
            {
                var signals = new List<ISignal>();

                for (int v = 0; v < result0.Signals.Count; v++)
                {
                    if (result0.Signals[v].BandwidthKhz >= _FilterMinBandWidthkHz && result0.Signals[v].BandwidthKhz <= _FilterMaxBandWidthkHz)
                    {
                        signals.Add(result0.Signals[v]);
                    }
                }
                result0 = new ProcessResult(signals);
            }

            return result0;
        }

        double[] MinFreqs = new double[] { 2385d };
        double[] MaxFreqs = new double[] { 2447, 5d };
        private bool FRSonRangeRI(double CenterFreq)
        {
            double[] localMinFreqs;
            lock (MinFreqs)
            {
                localMinFreqs = new double[MinFreqs.Count()];
                MinFreqs.CopyTo(localMinFreqs, 0);
            }

            double[] localMaxFreqs;
            lock (MaxFreqs)
            {
                localMaxFreqs = new double[MaxFreqs.Count()];
                MaxFreqs.CopyTo(localMaxFreqs, 0);
            }

            int Count = Math.Min(localMinFreqs.Count(), localMaxFreqs.Count());
            for (int i = 0; i < Count; i++)
            {
                if (CenterFreq >= localMinFreqs[i] && CenterFreq <= localMaxFreqs[i])
                    return true;
            }
            return false;
        }

        List<TableSignalsUAV> lTableSignalsUAVs = new List<TableSignalsUAV>();

        //TO DO: Work with DB 
        private void TableSignalsWork(ProcessResult result0)
        {
            List<double> Freqs = result0.Signals.Select(x => (double)x.FrequencyKhz).ToList<double>();

            TableSignalsUAV[] localTableSignalsUAVs = new TableSignalsUAV[0];
            lock (lTableSignalsUAVs)
            {
                localTableSignalsUAVs = lTableSignalsUAVs.ToArray();
            }

            List<ISignal> lSignals = new List<ISignal>(result0.Signals.ToList());

            //В цикле
            for (int j = 0; j < localTableSignalsUAVs.Count(); j++)
            {
                for (int k = 0; k < lSignals.Count(); k++)
                {
                    //Сравнить сигнал с каждым элементом в листе и обновить в базе
                    (float FrequencyKhz, float BandwidthKhz) signal1 = ((float)localTableSignalsUAVs[j].FrequencyKHz, localTableSignalsUAVs[j].BandKHz);
                    (float FrequencyKhz, float BandwidthKhz) signal2 = (lSignals[k].CentralFrequencyKhz, lSignals[k].BandwidthKhz);
                    if (CompareSignalsKhz(signal1, signal2))
                    {
                        TableResFF updateSignal = new()
                        {
                            Id = localTableSignalsUAVs[j].Id,
                            FrequencyKHz = lSignals[k].CentralFrequencyKhz,
                            BandKHz = lSignals[k].BandwidthKhz,
                            //Modulation = localTableSignalsUAVs[j].Modulation,
                            //ManipulationVelocity = localTableSignalsUAVs[j].ManipulationVelocity,
                            Level = localTableSignalsUAVs[j].Level,
                            Time = SignalTime[lSignals[k]],
                        };

                        ChangeRecordInTableSignalsUAVs(updateSignal);
                        lSignals.RemoveAt(k);
                        k--;
                    }
                }
            }

            //Добавить оставшиеся необновленные, значит новые, сигналы в базу
            for (int k = 0; k < lSignals.Count(); k++)
            {
                //TableResFF addSignal = new ()
                //{
                //    Id = localTableSignalsUAVs[k].Id,
                //    FrequencyKHz = lSignals[k].CentralFrequencyKhz,
                //    BandKHz = lSignals[k].BandwidthKhz,
                //    Modulation = localTableSignalsUAVs[k].Modulation,
                //    ManipulationVelocity = localTableSignalsUAVs[k].ManipulationVelocity,
                //    Level = localTableSignalsUAVs[k].Level,
                //    Time = SignalTime[lSignals[k]],
                //};
                TableResFF addSignal = new()
                {
                    Id = 0,
                    FrequencyKHz = lSignals[k].CentralFrequencyKhz,
                    BandKHz = lSignals[k].BandwidthKhz,
                    //Modulation = 0,
                    //ManipulationVelocity = 0,
                    Level = 0,
                    Time = SignalTime[lSignals[k]],
                };
                AddRecordToTableSignalsUAVs(addSignal);
            }

        }

        private void ChangeRecordInTableSignalsUAVs(TableResFF record)
        {
            try
            {
                //TO DO:
                //clientDB?.Tables[NameTable.TableSignalsUAV].Change(record);
                _dbClient.ChangeRecord(NameTable.TableResFF, record);
            }
            catch { }
        }

        private void AddRecordToTableSignalsUAVs(TableResFF record)
        {
            try
            {
                //TO DO:
                //clientDB?.Tables[NameTable.TableSignalsUAV].Add(record);
                _dbClient?.AddRecord(NameTable.TableResFF, record);
            }
            catch { }
        }

        public float EpsilonFrequencyKhz { get; set; } = 1000;
        public float EpsilonBandwidthKhz { get; set; } = 5000;
        private bool CompareSignalsKhz((float FrequencyKhz, float BandwidthKhz) signal1, (float FrequencyKhz, float BandwidthKhz) signal2)
        {
            if ((signal1.FrequencyKhz - EpsilonFrequencyKhz <= signal2.FrequencyKhz && signal2.FrequencyKhz <= signal1.FrequencyKhz + EpsilonFrequencyKhz)
                &&
                (signal1.BandwidthKhz - EpsilonBandwidthKhz <= signal2.BandwidthKhz && signal2.BandwidthKhz <= signal1.BandwidthKhz + EpsilonBandwidthKhz))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    internal class TableSignalsUAV
    {
        public float FrequencyKHz { get; set; }
        public float BandKHz { get; set; }
        public int Id { get; set; }
        public object System { get; set; }
        public object TimeStart { get; set; }
        public DateTime TimeStop { get; set; }
        public object Type { get; set; }
        public object TypeL { get; set; }
        public object TypeM { get; set; }
        public bool Correlation { get; set; }
        public byte Modulation { get; internal set; }
        public short ManipulationVelocity { get; internal set; }
        public short Level { get; internal set; }
    }


}
