﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServerKvetkaClass
{
    public static class Support
    {
        public static int PointCount(float MinFreqMHz, float MaxFreqMHz, int ResolutionHz)
        {
            if (ResolutionHz < 100) ResolutionHz = 100;
            return (int)((MaxFreqMHz - MinFreqMHz) * 1e6 / ResolutionHz);
        }
    }
}
