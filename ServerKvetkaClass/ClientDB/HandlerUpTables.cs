﻿using InheritorsEventArgs;
using KvetkaModelsDBLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServerKvetkaClass
{
    public partial class DbClient
    {
        private void HandlerUpData_ClientDb(object obj, DataEventArgs eventArgs)
        {
            Console.WriteLine($"Load data from Db. Tables {eventArgs.Name.ToString()} count records - {eventArgs.AbstractData.ListRecords.Count} \n");
        }

        private void HandlerError_ClientDb(object sender, OperationTableEventArgs e)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine($"{e.GetMessage}\n");
            Console.ResetColor();
        }

        private void HandlerUpdate_TableJammer(object sender, TableEventArgs<TableJammer> e)
        {
        }

        private void HandlerUpdate_TableFreqRangesElint(object sender, TableEventArgs<TableFreqRangesElint> e)
        {
        }

        private void HandlerUpdate_TableFreqRangesJamming(object sender, TableEventArgs<TableFreqRangesJamming> e)
        {
        }

        private void HandlerUpdate_TableFreqKnown(object sender, TableEventArgs<TableFreqKnown> e)
        {
        }

        private void HandlerUpdate_TableFreqImportant(object sender, TableEventArgs<TableFreqImportant> e)
        {
        }

        private void HandlerUpdate_TableFreqForbidden(object sender, TableEventArgs<TableFreqForbidden> e)
        {
        }

        private void HandlerUpdate_TableSectorsElint(object sender, TableEventArgs<TableSectorsElint> e)
        {
        }

        private void HandlerUpdate_TableADSB(object sender, TableEventArgs<TableADSB> e)
        {
        }

        private void HandlerUpdate_TableResFF(object sender, TableEventArgs<TableResFF> e)
        {
        }

        private void HandlerUpdate_TableTrackResFF(object sender, TableEventArgs<TableTrackResFF> e)
        {
        }

        private void HandlerUpdate_TableResFFJam(object sender, TableEventArgs<TableResFFJam> e)
        {
        }

        private void HandlerUpdate_TableResFHSS(object sender, TableEventArgs<TableResFHSS> e)
        {
        }

        private void HandlerUpdate_TableSubscriberResFHSS(object sender, TableEventArgs<TableSubscriberResFHSS> e)
        {
        }

        private void HandlerUpdate_TableTrackSubscriberResFHSS(object sender, TableEventArgs<TableTrackSubscriberResFHSS> e)
        {
        }

        private void HandlerUpdate_TableResFHSSJam(object sender, TableEventArgs<TableResFHSSJam> e)
        {
        }
    }
}
