﻿using GrpcClientLib;
using KvetkaModelsDBLib;

namespace ServerKvetkaClass
{
    public partial class DbClient
    {
        #region Fields
        private ServiceClient serviceClient;
        private string ip = "127.0.0.1";
        private int port = 30051;
        #endregion

        #region Properties
        public bool IsConnected { get; set; } = false;
        private string Name { get; set; } = "KvetkaServerApp";
        #endregion

        private void InitClientDB()
        {
            serviceClient.OnConnect += HandlerConnect_ClientDb;
            serviceClient.OnDisconnect += HandlerDisconnect_ClientDb;
            serviceClient.OnUpData += HandlerUpData_ClientDb;
            serviceClient.OnErrorDataBase += HandlerError_ClientDb;
            (serviceClient.Tables[NameTable.TableJammer] as ITableUpdate<TableJammer>).OnUpTable += HandlerUpdate_TableJammer;
            (serviceClient.Tables[NameTable.TableFreqRangesElint] as ITableUpdate<TableFreqRangesElint>).OnUpTable += HandlerUpdate_TableFreqRangesElint;
            (serviceClient.Tables[NameTable.TableFreqRangesJamming] as ITableUpdate<TableFreqRangesJamming>).OnUpTable += HandlerUpdate_TableFreqRangesJamming;
            (serviceClient.Tables[NameTable.TableFreqKnown] as ITableUpdate<TableFreqKnown>).OnUpTable += HandlerUpdate_TableFreqKnown;
            (serviceClient.Tables[NameTable.TableFreqImportant] as ITableUpdate<TableFreqImportant>).OnUpTable += HandlerUpdate_TableFreqImportant;
            (serviceClient.Tables[NameTable.TableFreqForbidden] as ITableUpdate<TableFreqForbidden>).OnUpTable += HandlerUpdate_TableFreqForbidden;
            (serviceClient.Tables[NameTable.TableSectorsElint] as ITableUpdate<TableSectorsElint>).OnUpTable += HandlerUpdate_TableSectorsElint;
            (serviceClient.Tables[NameTable.TableADSB] as ITableUpdate<TableADSB>).OnUpTable += HandlerUpdate_TableADSB;
            (serviceClient.Tables[NameTable.TableResFF] as ITableUpdate<TableResFF>).OnUpTable += HandlerUpdate_TableResFF;
            (serviceClient.Tables[NameTable.TableTrackResFF] as ITableUpdate<TableTrackResFF>).OnUpTable += HandlerUpdate_TableTrackResFF;
            (serviceClient.Tables[NameTable.TableResFFJam] as ITableUpdate<TableResFFJam>).OnUpTable += HandlerUpdate_TableResFFJam;
            (serviceClient.Tables[NameTable.TableResFHSS] as ITableUpdate<TableResFHSS>).OnUpTable += HandlerUpdate_TableResFHSS;
            (serviceClient.Tables[NameTable.TableSubscriberResFHSS] as ITableUpdate<TableSubscriberResFHSS>).OnUpTable += HandlerUpdate_TableSubscriberResFHSS;
            (serviceClient.Tables[NameTable.TableTrackSubscriberResFHSS] as ITableUpdate<TableTrackSubscriberResFHSS>).OnUpTable += HandlerUpdate_TableTrackSubscriberResFHSS;
            (serviceClient.Tables[NameTable.TableResFHSSJam] as ITableUpdate<TableResFHSSJam>).OnUpTable += HandlerUpdate_TableResFHSSJam;
        }


        private void DeInitClientDB()
        {
            serviceClient.OnConnect -= HandlerConnect_ClientDb;
            serviceClient.OnDisconnect -= HandlerDisconnect_ClientDb;
            serviceClient.OnUpData -= HandlerUpData_ClientDb;
            serviceClient.OnErrorDataBase -= HandlerError_ClientDb;
        }
    }
}
