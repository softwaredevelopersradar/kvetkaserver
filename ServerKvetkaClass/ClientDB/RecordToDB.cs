﻿using GrpcClientLib;
using InheritorsEventArgs;
using KvetkaModelsDBLib;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace ServerKvetkaClass
{
    public partial class DbClient
    {
        private static readonly object LockObject = new();

        public void AddRecord(NameTable Table, int nufOfRec)
        {
            try
            {
                Random rand = new Random();
                object record = null;
                switch (Table)
                {

                    case NameTable.TableJammer:
                        record = new TableJammer
                        {
                            Id = nufOfRec,
                            Coordinates = new Coord
                            {
                                Altitude = (float)rand.NextDouble() * rand.Next(0, 360),
                                Latitude = -53.56d,
                                Longitude = -26.365d
                            },
                            CallSign = "topol",
                            Note = "hello local",
                            Role = StationRole.Linked
                        };
                        break;
                    case NameTable.TableFreqForbidden:
                        record = new TableFreqForbidden
                        {
                            FreqMaxKHz = rand.Next(150000, 250000),
                            FreqMinKHz = rand.Next(30000, 50000),
                            NumberASP = nufOfRec,
                        };
                        break;
                    case NameTable.TableFreqKnown:
                        record = new TableFreqKnown
                        {
                            FreqMaxKHz = rand.Next(150000, 250000),
                            FreqMinKHz = rand.Next(30000, 50000),
                            NumberASP = nufOfRec
                        };
                        break;
                    case NameTable.TableADSB:
                        record = new TableADSB
                        {
                            ICAO = rand.Next().ToString()
                        };
                        break;
                    case NameTable.TableResFF:
                        record = new TableResFF
                        {
                            FrequencyKHz = rand.Next(),
                            //ListTrack = new System.Collections.ObjectModel.ObservableCollection<TableTrackResFF>() { new TableTrackResFF() { BearingOwn = rand.Next() }, new TableTrackResFF() { BearingOwn = rand.Next() } }
                        };
                        break;
                    case NameTable.TableResFFJam:
                        record = new TableResFFJam
                        {
                            FrequencyKHz = rand.Next(),
                            Threshold = (short)rand.Next(0, 255)
                        };
                        break;
                    case NameTable.TableResFHSS:
                        record = new TableResFHSS
                        {
                            FrequencyMinKHz = rand.Next(),
                            ListSubscribers = new ObservableCollection<TableSubscriberResFHSS>() {
                                new TableSubscriberResFHSS() { Time = DateTime.Now, Position = new TableTrackSubscriberResFHSS() { BearingOwn = rand.Next() } },
                                new TableSubscriberResFHSS() { Time = DateTime.Now, Position = new TableTrackSubscriberResFHSS() { BearingOwn = rand.Next() } }
                            }
                        };
                        break;
                    default:
                        break;
                }
                if (record != null)
                    serviceClient?.Tables[Table].Add(record);
            }
            catch (GrpcClientLib.ExceptionClient exceptClient)
            {
                ConsoleLogError(exceptClient.Message);
            }
        }

        public void AddRecord(NameTable Table, AbstractCommonTable abstractCommonTable)
        {
            serviceClient?.Tables[Table].Add(abstractCommonTable);
        }

        public void AddRecords(NameTable Table)
        {
            int nufOfRec = 0;
            Random rand = new Random();
            dynamic listForSend = new List<AbstractCommonTable>();

            switch (Table)
            {

                case NameTable.TableJammer:
                    listForSend = new List<TableJammer>();
                    for (int i = nufOfRec; i < nufOfRec + 3; i++)
                    {
                        listForSend.Add(new TableJammer
                        {
                            Coordinates = new Coord
                            {
                                Altitude = (float)rand.NextDouble() * rand.Next(0, 360),
                                Latitude = rand.NextDouble() * rand.Next(0, 360),
                                Longitude = rand.NextDouble() * rand.Next(0, 360)
                            }
                        });
                    };
                    break;
                case NameTable.TableFreqForbidden:
                    listForSend = new List<TableFreqForbidden>();
                    for (int i = nufOfRec; i < nufOfRec + 3; i++)
                    {
                        listForSend.Add(new TableFreqForbidden
                        {
                            FreqMaxKHz = rand.Next(150000, 250000),
                            FreqMinKHz = rand.Next(30000, 50000)
                        });
                    };
                    break;
                case NameTable.TableADSB:
                    listForSend = new List<TableADSB>();
                    for (int i = nufOfRec; i < nufOfRec + 3; i++)
                    {
                        listForSend.Add(new TableADSB
                        {
                            ICAO = rand.Next().ToString(),
                            Time = DateTime.Now
                        });
                    };
                    break;
                case NameTable.TableResFF:
                    listForSend = new List<TableResFF>();
                    for (int i = nufOfRec; i < nufOfRec + 3; i++)
                    {
                        listForSend.Add(new TableResFF
                        {
                            FrequencyKHz = rand.Next(),
                            //ListTrack = new System.Collections.ObjectModel.ObservableCollection<TableTrackResFF>() { new TableTrackResFF() { BearingOwn = rand.Next() }, new TableTrackResFF() { BearingOwn = rand.Next() } }
                        });
                    };
                    break;

                default:
                    break;
            }

            serviceClient?.Tables[Table].AddRange(listForSend);
        }

        public void ChangeRecord(NameTable Table, int nufOfRec)
        {
            try
            {
                Random rand = new Random();
                object record = null;
                switch (Table)
                {

                    case NameTable.TableJammer:
                        record = new TableJammer
                        {
                            Id = nufOfRec,
                            Coordinates = new Coord
                            {
                                Altitude = (float)rand.NextDouble() * rand.Next(0, 360),
                                Latitude = rand.Next(0, 360),
                                Longitude = rand.Next(0, 360)
                            },
                            CallSign = "berezka",
                            Note = "bye local",
                            Role = StationRole.Complex
                        };
                        break;
                    case NameTable.TableFreqForbidden:
                        record = new TableFreqForbidden
                        {
                            FreqMaxKHz = rand.Next(150000, 250000),
                            FreqMinKHz = rand.Next(30000, 50000),
                            NumberASP = nufOfRec
                        };
                        break;
                    case NameTable.TableFreqKnown:
                        record = new TableFreqKnown
                        {
                            FreqMaxKHz = rand.Next(150000, 250000),
                            FreqMinKHz = rand.Next(30000, 50000),
                            NumberASP = nufOfRec
                        };
                        break;
                    case NameTable.TableADSB:
                        record = new TableADSB
                        {
                            ICAO = nufOfRec.ToString(),
                            Time = DateTime.Now
                        };
                        break;
                    case NameTable.TableResFF:
                        record = new TableResFF
                        {
                            Id = nufOfRec,
                            FrequencyKHz = rand.Next(),
                            //ListTrack = new System.Collections.ObjectModel.ObservableCollection<TableTrackResFF>() { new TableTrackResFF() { BearingOwn = rand.Next() }, new TableTrackResFF() { BearingOwn = rand.Next() } }
                        };
                        break;
                    case NameTable.TableResFFJam:
                        record = new TableResFFJam
                        {
                            Id = nufOfRec,
                            FrequencyKHz = rand.Next(),
                            Threshold = (short)rand.Next(0, 255)
                        };
                        break;
                    default:
                        break;
                }
                if (record != null)
                    serviceClient?.Tables[Table].Change(record);
            }
            catch (GrpcClientLib.ExceptionClient exceptClient)
            {
                ConsoleLogError(exceptClient.Message);
            }
        }

        public void ChangeRecord(NameTable Table, AbstractCommonTable abstractCommonTable)
        {
            try
            {
                serviceClient?.Tables[Table].Change(abstractCommonTable);
            }
            catch (GrpcClientLib.ExceptionClient exceptClient)
            {
                ConsoleLogError(exceptClient.Message);
            }
        }

        public void ChangeRecords(NameTable Table)
        {
            Random rand = new Random();
            int nufOfRec = 0;
            dynamic listForSend = new List<AbstractCommonTable>();

            switch (Table)
            {

                case NameTable.TableJammer:
                    listForSend = new List<TableJammer>();
                    for (int i = nufOfRec; i < nufOfRec + 3; i++)
                    {
                        listForSend.Add(new TableJammer
                        {
                            Id = i,
                            Coordinates = new Coord
                            {
                                Altitude = (float)rand.NextDouble() * rand.Next(0, 360),
                                Latitude = rand.NextDouble() * rand.Next(0, 360),
                                Longitude = rand.NextDouble() * rand.Next(0, 360)
                            }
                        });
                    };
                    break;
                case NameTable.TableFreqForbidden:
                    listForSend = new List<TableFreqForbidden>();
                    for (int i = nufOfRec; i < nufOfRec + 3; i++)
                    {
                        listForSend.Add(new TableFreqForbidden
                        {
                            Id = i,
                            FreqMaxKHz = rand.Next(150000, 250000),
                            FreqMinKHz = rand.Next(30000, 50000)
                        });
                    };
                    break;
                case NameTable.TableADSB:
                    listForSend = new List<TableADSB>();
                    for (int i = nufOfRec; i < nufOfRec + 3; i++)
                    {
                        listForSend.Add(new TableADSB
                        {
                            ICAO = i.ToString(),
                            Time = DateTime.Now
                        });
                    };
                    break;
                case NameTable.TableResFF:
                    listForSend = new List<TableResFF>();
                    for (int i = nufOfRec; i < nufOfRec + 3; i++)
                    {
                        listForSend.Add(new TableResFF
                        {
                            Id = i,
                            FrequencyKHz = rand.Next(),
                            //ListTrack = new System.Collections.ObjectModel.ObservableCollection<TableTrackResFF>() { new TableTrackResFF() { BearingOwn = rand.Next() }, new TableTrackResFF() { BearingOwn = rand.Next() } }
                        });
                    };
                    break;

                default:
                    break;
            }

            serviceClient?.Tables[Table].ChangeRange(listForSend);
        }

        public void DeleteRecord(NameTable Table, int nufOfRec)
        {
            try
            {
                object record = null;
                switch (Table)
                {

                    case NameTable.TableJammer:
                        record = new TableJammer
                        {
                            Id = nufOfRec
                        };
                        break;
                    case NameTable.TableFreqForbidden:
                        record = new TableFreqForbidden
                        {
                            Id = nufOfRec
                        };
                        break;
                    case NameTable.TableFreqKnown:
                        record = new TableFreqKnown
                        {
                            Id = nufOfRec
                        };
                        break;
                    case NameTable.TableADSB:
                        record = new TableADSB
                        {
                            ICAO = nufOfRec.ToString(),
                        };
                        break;
                    case NameTable.TableResFF:
                        record = new TableResFF
                        {
                            Id = nufOfRec
                        };
                        break;
                    case NameTable.TableResFFJam:
                        record = new TableResFFJam
                        {
                            Id = nufOfRec
                        };
                        break;
                    case NameTable.TableResFHSS:
                        record = new TableResFHSS
                        {
                            Id = nufOfRec
                        };
                        break;
                    default:
                        break;
                }
                if (record != null) serviceClient?.Tables[Table].Delete(record);
            }
            catch (GrpcClientLib.ExceptionClient exceptClient)
            {
                ConsoleLogError(exceptClient.Message.ToString());
            }
        }

        public void DeleteRecords(NameTable Table, IEnumerable<int> nufOfRecs)
        {
            dynamic listForSend = new List<AbstractCommonTable>();

            switch (Table)
            {

                case NameTable.TableJammer:
                    listForSend = new List<TableJammer>();
                    for (int i = 0; i < nufOfRecs.Count(); i++)
                    {
                        listForSend.Add(new TableJammer
                        {
                            Id = nufOfRecs.ElementAt(i)
                        });
                    };
                    break;
                case NameTable.TableFreqForbidden:
                    listForSend = new List<TableFreqForbidden>();
                    for (int i = 0; i < nufOfRecs.Count(); i++)
                    {
                        listForSend.Add(new TableFreqForbidden
                        {
                            Id = nufOfRecs.ElementAt(i)
                        });
                    };
                    break;

                default:
                    break;
            }

            serviceClient?.Tables[Table].RemoveRange(listForSend);
        }

        public async void LoadFromTable(NameTable Table)
        {
            try
            {
                List<AbstractCommonTable> table = new List<AbstractCommonTable>();

                if (serviceClient.Tables[Table] is IDependentAsp)
                {
                    //dynamic tablDepenAsp = await (serviceClient.Tables[NameTable.TableFreqKnown] as IDependentAsp).LoadByFilterAsync<FreqRanges>(nufOfRec);
                    //DispatchIfNecessary(() =>
                    //{
                    //    tbMessage.AppendText($"Load data from Db. {((NameTable)Tables.SelectedItem).ToString()} count records - {tablDepenAsp.Count} \n");
                    //});
                }
                else
                {
                    //table = await serviceClient?.Tables[nameTable].LoadAsync<AbstractCommonTable>();
                    table = serviceClient?.Tables[Table].Load<AbstractCommonTable>();
                    Console.WriteLine($"Load data from Db. {Table} count records - {table.Count} \n");
                }
            }
            catch (ExceptionClient exeptClient)
            {
                ConsoleLogError(exeptClient.Message);
            }
            catch (ExceptionDatabase excpetService)
            {
                ConsoleLogError(excpetService.Message);
            }
        }

        public void ClearTable(NameTable Table)
        {
            try
            {
                serviceClient?.Tables[Table].Clear();
            }
            catch (GrpcClientLib.ExceptionClient exceptClient)
            {
                ConsoleLogError(exceptClient.ToString());
            }
        }

        private static void ConsoleLogError(string errorLine)
        {
            lock (LockObject)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(errorLine);
                Console.ResetColor();
            }
        }

    }
}
