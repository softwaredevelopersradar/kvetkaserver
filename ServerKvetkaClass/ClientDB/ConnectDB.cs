﻿using GrpcClientLib;
using InheritorsEventArgs;
using KvetkaModelsDBLib;
using System;

namespace ServerKvetkaClass
{
    public partial class DbClient
    {
        public DbClient()
        {
            ip = "127.0.0.1";
            port = 30051;
        }

        public DbClient(string ip, int port)
        {
            this.ip = ip;
            this.port = port;
        }

        public void Connect()
        {
            try
            {
                Console.WriteLine($"DB connect on {ip}:{port}");
                serviceClient = new ServiceClient(Name, ip, port);
                InitClientDB();
                serviceClient.Connect();
            }
            catch (ExceptionClient exceptClient)
            {
                ConsoleLogError(exceptClient.ToString());
            }
        }

        public void Disconnect()
        {
            try
            {
                DeInitClientDB();
                serviceClient?.Disconnect();
            }
            catch (ExceptionClient exceptClient)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(exceptClient);
                Console.ResetColor();
            }
        }

        private async void HandlerConnect_ClientDb(object sender, ClientEventArgs e)
        {
            IsConnected = true;

            foreach (NameTable table in Enum.GetValues(typeof(NameTable)))
            {
                var tempTable = await serviceClient.Tables[table].LoadAsync<AbstractCommonTable>();
                Console.WriteLine($"Load data from Db. {table.ToString()} count records - {tempTable.Count} \n");
            }
        }

        private void HandlerDisconnect_ClientDb(object obj, ClientEventArgs eventArgs)
        {
            IsConnected = false;

            if (eventArgs.GetMessage != "")
            {
                ConsoleLogError(eventArgs.GetMessage);
            }
            serviceClient = null;
        }
    }
}
