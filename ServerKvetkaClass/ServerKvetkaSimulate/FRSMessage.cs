﻿using AWPtoDSPprotocolKvetka;
using KvetkaProtocol;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace ServerKvetkaClass
{
    public partial class ServerKvetkaSimulate : IServerController
    {
        public override FRSMessage GenerateFRSMessage(int N = 3)
        {
            FRSPackage[] frsPackages = GenerateFRSPackages(N);

            int InformationLength = 1 + frsPackages.Length * FRSPackage.BinarySize;
            MessageHeader header = new MessageHeader(0, 0, 0, 0, InformationLength);
            FRSMessage fRSMessage = new FRSMessage(header, 0, frsPackages);

            return fRSMessage;
        }

        private FRSPackage[] GenerateFRSPackages(int N)
        {
            Random random = new Random();

            FRSPackage[] fRSPackages = new FRSPackage[N];

            for (int i = 0; i < fRSPackages.Length; i++)
            {
                fRSPackages[i] = new FRSPackage()
                {
                    ID = i,
                    FrequencykHzX10 = (int)((random.NextDouble() * (MaxFreqMHz - MinFreqMHz) + MinFreqMHz) * 1e4),
                    Level = (byte)random.Next(0, 120),
                };
            }

            return fRSPackages;
        }

        public override void Control(FRSMessage fRSMessage)
        {
            fRSMessage.CycleIndication = 0;
            foreach (FRSPackage fRSPackage in fRSMessage.FRSPackages)
            {
                fRSPackage.Control = 1;
                fRSPackage.Suppression = 0;
                fRSPackage.Radiation = 0;
            }
        }

        public override void Jamming(FRSMessage fRSMessage)
        {
            fRSMessage.CycleIndication = 1;
            foreach (FRSPackage fRSPackage in fRSMessage.FRSPackages)
            {
                fRSPackage.Control = 0;
                fRSPackage.Suppression = 1;
                fRSPackage.Radiation = 1;
            }
        }
    }
}
