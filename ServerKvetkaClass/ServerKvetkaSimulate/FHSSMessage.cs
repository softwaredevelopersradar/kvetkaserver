﻿using AWPtoDSPprotocolKvetka;
using KvetkaProtocol;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace ServerKvetkaClass
{
    public partial class ServerKvetkaSimulate : IServerController
    {
        public override FHSSMessage GenerateFHSSMessage(int N = 4)
        {
            FHSSPackage[] fhssPackages = GenerateFHSSPackages(N);

            int InformationLength = 1 + fhssPackages.Select(x => x.StructureBinarySize).Sum();
            MessageHeader header = new MessageHeader(0, 0, 0, 0, InformationLength);
            FHSSMessage fhssMessage = new FHSSMessage(header, (byte)N, fhssPackages);

            return fhssMessage;
        }

        private FHSSPackage[] GenerateFHSSPackages(int N)
        {
            Random random = new();

            FHSSPackage[] fhssPackages = new FHSSPackage[N];

            for (int i = 0; i < fhssPackages.Length; i++)
            {
                byte fhhsPeeksCount = (byte)random.Next(0, 256);

                int[] FrequencieskHzX10 = GenerateFrequencieskHzX10(fhhsPeeksCount);

                fhssPackages[i] = new FHSSPackage()
                {
                    ID = (byte)i,
                    Level = (byte) random.Next(0,120),
                    FreqsCount = fhhsPeeksCount,
                    FrequencieskHzX10 = FrequencieskHzX10
                };
            }

            return fhssPackages;
        }

        private int[] GenerateFrequencieskHzX10(byte fhhsPeeksCount)
        {
            Random random = new();

            int FrequencykHzX10v1 = (int)((random.NextDouble() * (MaxFreqMHz - MinFreqMHz) + MinFreqMHz) * 1e4);
            int FrequencykHzX10v2 = (int)((random.NextDouble() * (MaxFreqMHz - MinFreqMHz) + MinFreqMHz) * 1e4);

            int MinFrequencykHzX10 = Math.Min(FrequencykHzX10v1, FrequencykHzX10v2);
            int MaxFrequencykHzX10 = Math.Max(FrequencykHzX10v1, FrequencykHzX10v2);

            int[] FrequencieskHzX10 = GenerateNetwork(MinFrequencykHzX10, MaxFrequencykHzX10, fhhsPeeksCount);

            return FrequencieskHzX10;
        }

        private int[] GenerateNetwork(int minFrequencykHzX10, int maxFrequencykHzX10, byte fhhsPeeksCount)
        {
            Random random = new();
            int[] FrequencieskHzX10 = new int[fhhsPeeksCount];
            for (int i = 0; i < fhhsPeeksCount; i++)
            {
                FrequencieskHzX10[i] = (int)((random.NextDouble() * (maxFrequencykHzX10 - minFrequencykHzX10) + minFrequencykHzX10));
            }
            return FrequencieskHzX10;
        }

        public override void Control(FHSSMessage fhssMessage)
        {
            foreach (FHSSPackage fhssPackage in fhssMessage.FHSSPackages)
            {
                fhssPackage.State = 0;
                fhssPackage.Level = 0;
                fhssPackage.FreqsCount = 1;
                fhssPackage.FrequencieskHzX10 = new int[] { 0 };
            }
            fhssMessage.Header.InformationLength = 1 + fhssMessage.FHSSPackages.Select(x => x.StructureBinarySize).Sum();
        }

        public override void Jamming(FHSSMessage fhssMessage)
        {
            Random random = new();

            foreach (FHSSPackage fhssPackage in fhssMessage.FHSSPackages)
            {
                byte fhssPeeksCount = (byte)random.Next(0, 256);

                fhssPackage.State = 1;
                fhssPackage.Level = (byte)random.Next(0, 120);
                fhssPackage.FreqsCount = fhssPeeksCount;
                fhssPackage.FrequencieskHzX10 = GenerateFrequencieskHzX10(fhssPeeksCount);
            }

            fhssMessage.Header.InformationLength = 1 + fhssMessage.FHSSPackages.Select(x => x.StructureBinarySize).Sum();
        }

    }
}
