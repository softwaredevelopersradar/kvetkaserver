﻿using AWPtoDSPprotocolKvetka;
using KvetkaProtocol;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ServerKvetkaClass
{
    public partial class ServerKvetkaSimulate : IServerController
    {
        public ServerKvetkaSimulate(KvetkaServer kvetkaServer) : base(kvetkaServer)
        {
        }

        public override async Task<KvetkaServerMode> ModeSwitch(KvetkaServerMode requestServerMode, CancellationToken token)
        {
            switch (requestServerMode)
            {
                case KvetkaServerMode.Stop:
                    break;
                case KvetkaServerMode.RadioIntelligence:
                    await RadioIntelligenceLoop(token);
                    break;
                case KvetkaServerMode.RadioJammingFrs:
                    await RadioJammingFrsLoop(token);
                    break;
                case KvetkaServerMode.RadioJammingFhss:
                    await RadioJammingFhssLoop(token);
                    break;
                default:
                    break;
            }
            return requestServerMode;
        }

        public override byte[] GenerateSpectrum(int Count = 3000)
        {
            Random random = new();

            byte[] spectrum = new byte[Count];

            return spectrum.Select(x => x = (byte)(80 + random.Next(0, 20))).ToArray();
        }

        public override Slice[] GenerateSlices(byte SlicesCount, int StepHz)
        {
            int AmplitudesCount = Support.PointCount(MinFreqMHz, MaxFreqMHz, StepHz);

            Slice[] Slices = new Slice[SlicesCount];

            for (int i = 0; i < SlicesCount; i++)
            {
                Slices[i] = new Slice()
                {
                    Count = AmplitudesCount,
                    Amplitudes = GenerateApmlitudes(AmplitudesCount),
                };
            }

            return Slices;
        }

        private byte[] GenerateApmlitudes(int amplitudesCount)
        {
            Random random = new Random();

            byte[] Amplitudes = new byte[amplitudesCount];

            Amplitudes = Amplitudes.Select(x => x = (byte)(random.Next(0, 120))).ToArray();

            return Amplitudes;
        }
    }
}