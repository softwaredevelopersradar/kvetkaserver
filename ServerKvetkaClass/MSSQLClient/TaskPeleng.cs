﻿using System;

namespace ServerKvetkaClass
{
    public class TaskPeleng
    {
        /// <summary>
        /// Дата и время создания
        /// </summary>
        public DateTime DateCreate { get; }
        /// <summary>
        /// Уникальный идентификатор создателя
        /// </summary>
        public int IdUserCreate { get; }
        /// <summary>
        /// Уникальный идентификатор исполнителя
        /// </summary>
        public int ForIdUser { get; }
        /// <summary>
        /// Начальные частоты  поддиапазонов %2 == 0
        /// </summary>
        public byte[] Freq_b_MHz { get; }
        /// <summary>
        /// Конечные частоты поддиапазонов %2 == 0
        /// </summary>
        public byte[] Freq_e_MHz { get; }
        /// <summary>
        /// Фильтр
        /// </summary>
        public float Filter_kHz { get; }
        /// <summary>
        /// Дата и время начала выполнения задачи
        /// </summary>
        public DateTime Time_b { get; }
        /// <summary>
        /// Дата и время окончания выполнения задачи
        /// </summary>
        public DateTime Time_e { get; }
        /// <summary>
        /// Порог
        /// </summary>
        public float Porog_dB { get; }
        /// <summary>
        /// Аттенюатор
        /// </summary>
        public float Att { get; }
        /// <summary>
        /// Уникальный идентификатор ПК (2 всегда)
        /// </summary>
        public int IdPost { get; } = 2;
        /// <summary>
        /// Статус задачи
        /// </summary>
        public int Status { get; }
        /// <summary>
        /// Наименование задачи
        /// </summary>
        public string[] Name { get; }
        /// <summary>
        /// Уникальный идентификатор задания
        /// </summary>
        public int IdZadanie { get; }
        /// <summary>
        /// Комментарий к задаче
        /// </summary>
        public string[] Comment { get; }
        /// <summary>
        /// Длительность измерения
        /// </summary>
        public int DeltaIzm_seconds { get; }
        /// <summary>
        /// Интервал записи
        /// </summary>
        public int IntervalZap_seconds { get; }
        /// <summary>
        /// Уникальный идентификатор задачи из таблицы TASK_EX
        /// </summary>
        public int IdTaskEx { get; }
        /// <summary>
        /// Параметры сектора для задачи Поиска
        /// </summary>
        public int AzimuthMiddle_degrees { get; }
        /// <summary>
        /// Параметры сектора для задачи Поиска
        /// </summary>
        public int Sector_degrees { get; }
        /// <summary>
        /// Тип волны (0- земная,1- ионосферная)
        /// </summary>
        public int WaveType { get; }

        public TaskPeleng(DateTime dateCreate, int idUserCreate, int forIdUser, byte[] freq_b_MHz, byte[] freq_e_MHz, float filter_kHz,
                                                                    DateTime time_b, DateTime time_e, float porog_dB, float att, int idPost,
                                                                        int status, string[] name, int idZadanie, string[] comment, int delta_izm_seconds,
                                                                             int intervat_zap_seconds, int idTaskEx, int azimuthMiddle_degrees, int sector_degrees, int waveType)
        {
            DateCreate = dateCreate;
            IdUserCreate = idUserCreate;
            ForIdUser = forIdUser;
            Freq_b_MHz = freq_b_MHz;
            Freq_e_MHz = freq_e_MHz;
            Filter_kHz = filter_kHz;
            Time_b = time_b;
            Time_e = time_e;
            Porog_dB = porog_dB;
            Att = att;
            IdPost = idPost;
            Status = status;
            Name = name;
            IdZadanie = idZadanie;
            Comment = comment;
            DeltaIzm_seconds = delta_izm_seconds;
            IntervalZap_seconds = intervat_zap_seconds;
            IdTaskEx = idTaskEx;
            AzimuthMiddle_degrees = azimuthMiddle_degrees;
            Sector_degrees = sector_degrees;
            WaveType = waveType;
        }
    }
}
