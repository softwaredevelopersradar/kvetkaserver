﻿using Microsoft.Data.SqlClient;
using System;
using System.Data;
using System.Threading.Tasks;

namespace ServerKvetkaClass
{
    public class SQLClient
    {
        //Server=адрес_сервера;Database=имя_базы_данных;User Id=логин;Password=пароль; Подключение по паролю
        //Server=адрес_сервера;Database=имя_базы_данных;Trusted_Connection=True; Доверенное подключение
        //В качестве альтернативного названия параметра Server можно использовать Data Source, Address, Addr и NetworkAddress
        //string connectionString = "NetworkAddress=127.0.0.1;Database=master;Trusted_Connection=True;";

        SqlConnection connection;

        /// <summary>
        /// Подключение к БД пеленгатора
        /// </summary>
        /// <param name="connectionString">"NetworkAddress=127.0.0.1;Database=master;Trusted_Connection=True;"</param>
        /// <returns>Результат подключения</returns>
        public async Task<bool> ConnectPelengatorDB(string connectionString)
        {
            if (connection == null)
            {
                connection = new SqlConnection(connectionString);
            }
            else if (connection.State == ConnectionState.Open)
            {
                return true;
            }
            else
            {
                connection = null;
                return false;
            }

            try
            {
                // Открываем подключение
                await connection.OpenAsync();
                Console.WriteLine("Connection to the direction finder database is successful");
                return true;
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
                connection = null;
                return false;
            }
        }

        /// <summary>
        /// Отключение от БД пеленгатора
        /// </summary>
        /// <returns>Результат отключения</returns>
        public async Task<bool> DisconnectPelengatorDB()
        {
            try
            {
                // если подключение открыто
                if (connection.State == ConnectionState.Open)
                {
                    // закрываем подключение
                    await connection.CloseAsync();
                    Console.WriteLine("Disconnecting from the direction finder database successfully");
                }

                if (connection != null)
                    connection = null;

                return true;
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
                connection = null;
                return false;
            }
        }

        public async Task<bool> TaskPelengDB(TaskPeleng taskPeleng)
        {
            try
            {
                if (connection.State == ConnectionState.Closed)
                    return false;

                string sqlExpression = "INSERT INTO ZADACHA_EMO (DATECREATE, ID_USERCREATE, Freq_b, Freq_e, Filter, time_b, time_e, porog, att, ID_POST," +
                    " STATUS, NAME, ID_ZADANIE, COMMENT, Delta_izm, interval_zap, Params, azimuth_middle, sector)" +
                    $" VALUES ({taskPeleng.DateCreate}, {taskPeleng.IdUserCreate}, {FormFreqArray(taskPeleng.Freq_b_MHz)}, '{FormFreqArray(taskPeleng.Freq_e_MHz)}," +
                    $" {taskPeleng.Filter_kHz}, {taskPeleng.Time_b}, {taskPeleng.Time_e}, {taskPeleng.Porog_dB}, {taskPeleng.Att}, {taskPeleng.IdPost}, {taskPeleng.Status}," +
                    $" {taskPeleng.Name}, {taskPeleng.IdZadanie}, {taskPeleng.Comment}, {taskPeleng.DeltaIzm_seconds}, {taskPeleng.IntervalZap_seconds}, {taskPeleng.IdTaskEx}," +
                    $" {taskPeleng.AzimuthMiddle_degrees}, {taskPeleng.Sector_degrees})";

                SqlCommand command = new SqlCommand(sqlExpression, connection);

                int number = await command.ExecuteNonQueryAsync();// number - число строк добавленных в базу

                return true;
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }


        }

        /// <summary>
        /// Создание массива частот для пеленга
        /// </summary>
        /// <param name="arrayFreq">Массив частот</param>
        /// <returns>Стринг массив частот в формате "freq[i];freq[i+1]"</returns>
        private static string[] FormFreqArray(byte[] arrayFreq)
        {
            int length;

            if (arrayFreq.Length % 2 == 0)
                length = arrayFreq.Length;
            else if (arrayFreq.Length != 1)
                length = arrayFreq.Length - 1;
            else
                return null;

            var pairedFreq = new string[length / 2];

            for (int i = 0, y = 0; i < length; i += 2, y++)
            {
                pairedFreq[y] = arrayFreq[i].ToString() + ";" + arrayFreq[i + 1].ToString();
            }

            return pairedFreq;
        }
    }
}
